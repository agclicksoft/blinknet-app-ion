import { FeedMaintencePage } from './../pages/feed-maintence/feed-maintence';
import { RequestsPage } from './../pages/requests/requests';
import { FeedPage } from './../pages/feed/feed';
import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { IonicStorageModule } from "@ionic/storage";
import { RlTagInputModule } from "angular2-tag-input/dist";
import { Camera } from "@ionic-native/camera";
import { SocialSharing } from "@ionic-native/social-sharing";

//import {Ionic2MaskDirective} from "ionic2-mask-directive";

import { MyApp } from "./app.component";
import { AuthProvider } from "../providers/auth/auth";
import { UserProvider } from "../providers/user/user";
import { UtilProvider } from "../providers/util/util";
import { DirectivesModule } from "../directives/directives.module";
import { StateProvider } from "../providers/state/state";
import { InterceptorModule, HttpsRequestInterceptor } from "./modules/interceptor-module";
import { CallNumber } from "@ionic-native/call-number";
import { Geolocation } from "@ionic-native/geolocation";
import { AddressProvider } from "../providers/address/address";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { BrMaskerModule } from "brmasker-ionic-3";
import { LoadingProvider } from "../providers/loading/loading";
import { FeedProvider } from '../providers/feed/feed';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { RequestsProvider } from '../providers/requests/requests';
import { CommonProvider } from '../providers/common/common';


@NgModule({
  declarations: [MyApp, FeedMaintencePage, RequestsPage],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    RlTagInputModule,
    DirectivesModule,
    InterceptorModule,
    BrMaskerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp, FeedMaintencePage, RequestsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    UserProvider,
    UtilProvider,
    CommonProvider,
    Camera,
    StateProvider,
    SocialSharing,
    CallNumber,
    Geolocation,
    AddressProvider,
    FileTransfer,
    FileTransferObject,
    LoadingProvider,
    FeedProvider,
    AndroidFullScreen,
    RequestsProvider,
    { provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true }
  ]
})
export class AppModule {}
