import { StateModel } from "./state";
import { DistrictModel } from "./district";
import { CityModel } from "./city";
import { UtilProvider } from "../../providers/util/util";

export class Address {
  id?: any;
  zip_code?: string;
  parents?: string;
  state: string;
  street?: string;
  number?: string;
  complement?: string;
  city: string;
  neighborhood: string;
  created_at?: Date;
  updated_at?: Date;
  pivot?: any;
}

export class AddressModel {
  id?: number;
  zip_code?: string;
  street?: string;
  complement?: string;
  house_number?: string;
  state?: any;
  state_id?: string;
  state_name?: string;
  city?: any;
  city_id?: string;
  city_name?: string;
  district?: DistrictModel;
  district_id?: string;
  district_name?: string;
  person_id?: number;
  number?: string;

  public static GetNewInstance(): AddressModel {
    return new AddressModel(null, null, null, null, null);
  }

  public static ParseFromObject(object): AddressModel {
    const model = AddressModel.GetNewInstance();
    const util = new UtilProvider();

    if (!object) return model;

    model.id = object.id;
    model.zip_code = object.zip_code;
    model.street = object.street;
    model.complement = object.complement;
    model.person_id = object.person_id;
    model.state_id = object.state_id;
    model.city_id = object.city_id;
    model.district_id = object.district_id;
    model.house_number = object.house_number;

    if (util.isSet(object.state)) {
      model.state = StateModel.ParseFromObject(object.state);
    }

    if (util.isSet(object.city)) {
      model.city = CityModel.ParseFromObject(object.city);
    }

    if (util.isSet(object.district)) {
      model.district = DistrictModel.ParseFromObject(object.district);
    }

    return model;
  }

  constructor(
    id: any,
    zip_code: string,
    street: string,
    complement: string,
    number: string
  ) {
    this.id = id;
    this.zip_code = zip_code;
    this.street = street;
    this.house_number = number;
    this.complement = complement;
    this.state = StateModel.GetNewInstance();
    this.city = CityModel.GetNewInstance();
    this.district = DistrictModel.GetNewInstance();
    this.person_id = null;
  }
}
