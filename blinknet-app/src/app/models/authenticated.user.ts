import { PersonModel } from "./person";
import { UtilProvider } from "../../providers/util/util";

export class AuthenticatedUser {
  email: string;
  id: number;
  token: string;
  name: string;
  cel: string;  
  person_buyer?: PersonModel;
  person_provider?: PersonModel;
  person_corporate?: PersonModel;
  avatar_base64: string;

  public static GetNewInstance(): AuthenticatedUser {
    return new AuthenticatedUser(null, null, null, null);
  }

  public static ParseFromObject(object): AuthenticatedUser {
    const model = AuthenticatedUser.GetNewInstance();
    const util = new UtilProvider();

    if (object) {
      model.email = object.email;
      model.id = object.id;
      model.token = object.token;
      model.name = object.name;
      model.cel = object.cel;
      model.avatar_base64 = object.avatar_base64;

      if (util.isSet(object.person_provider)) {
        model.person_provider = PersonModel.ParseFromObject(object.person_provider);
      } else {
        model.person_provider = PersonModel.GetNewInstance();
      }
  
      if (util.isSet(object.person_buyer)) {
        model.person_buyer = PersonModel.ParseFromObject(object.person_buyer);
      } else {
        model.person_buyer = PersonModel.GetNewInstance();
      }

      if (util.isSet(object.person_corporate)) {
        model.person_corporate = PersonModel.ParseFromObject(object.person_corporate);
      } else {
        model.person_corporate = PersonModel.GetNewInstance();
      }
    }

    return model;
  }

  constructor(email: string, id: number, token: string, name: string) {
    this.email = email;
    this.id = id;
    this.token = token;
    this.name = name;
    this.person_provider = PersonModel.GetNewInstance();
    this.person_buyer = PersonModel.GetNewInstance();
    this.person_corporate = PersonModel.GetNewInstance();
  }
}