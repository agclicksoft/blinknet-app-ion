export class BranchOfActivityModel {
  id: number;
  name: string;

  public static GetNewInstance(): BranchOfActivityModel {
    return new BranchOfActivityModel(null, null);
  }

  public static ParseFromObject(object): BranchOfActivityModel {
    const model = BranchOfActivityModel.GetNewInstance();

    if (!object) return model;
    
    model.id = object.id;
    model.name = object.name;

    return model;
  }

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}
