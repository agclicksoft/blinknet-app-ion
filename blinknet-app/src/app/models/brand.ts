export class BrandModel {
  id: number;
  name: string;

  public static GetNewInstance(): BrandModel {
    return new BrandModel(null, null);
  }

  public static ParseFromObject(object): BrandModel {
    const model = BrandModel.GetNewInstance();

    if (object) {
      model.id = object.id;
      model.name = object.name;
    }

    return model;
  }

  constructor(id: number, name: string,) {
    this.id = id;
    this.name = name;
  }
}
