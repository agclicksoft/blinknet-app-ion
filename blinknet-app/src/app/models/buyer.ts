import { BranchOfActivityModel } from "./branch-of-activity";
import { UtilProvider } from "../../providers/util/util";

export class BuyerModel {
  id: number;
  branch_of_activities: BranchOfActivityModel[];
  number_of_boxes?: number;

  public static GetNewInstance(): BuyerModel {
    return new BuyerModel(null, []);
  }

  public static ParseFromObject(object): BuyerModel {
    const model = BuyerModel.GetNewInstance();
    let util = new UtilProvider();

    if (!object) return model;

    model.id = object.id;
    model.number_of_boxes = object.number_of_boxes || null;

    if (
      util.isSet(object.branch_of_activities) &&
      Array.isArray(object.branch_of_activities)
    ) {
      model.branch_of_activities = object.branch_of_activities.map(o =>
        BranchOfActivityModel.ParseFromObject(o)
      );
    } else {
      model.branch_of_activities = [];
    }

    return model;
  }

  constructor(id: number, branch_of_activities: BranchOfActivityModel[]) {
    this.id = id;
    this.branch_of_activities = branch_of_activities;
    this.number_of_boxes = null;
  }
}
