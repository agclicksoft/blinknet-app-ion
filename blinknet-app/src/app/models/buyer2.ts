import { User } from "./user2";
import { Product } from "./product";
import { Category } from "./category";
import { Address } from "./address";

export class Buyer2 {
    activity_field_id: Array<Object>;
    products: Array<Product> = [new Product];
    categories: Array<any> = [new Category];
    created_at: string;
    id: number;
    operatingRegions: any = [];
    tel: string;
    user_id: number;
    updated_at: string;
    user: User;
    adresses: Array<Address> = [];

}