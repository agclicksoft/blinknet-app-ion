export class Cep {

    cep: string;
    logradouro: string;
    complemento: string;
    bairro: string;
    localidade: string;
    uf: string;
    unidade: any;
    ibge: string;
    gia: string;

    constructor() {
        this.cep = ""
        this.logradouro = ""
        this.complemento = ""
        this.localidade = ""
        this.uf = ""
        this.unidade = ""
    }
}