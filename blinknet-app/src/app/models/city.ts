export class CityModel {
  id: string;
  name: string;
  state_id: number;

  public static GetNewInstance(): CityModel {
    return new CityModel(null, null, null);
  }

  public static ParseFromObject(object): CityModel {
    const model = CityModel.GetNewInstance();

    if (object) {
      model.id = object.id;
      model.name = object.name;
      model.state_id = object.state_id;
    }

    return model;
  }

  constructor(id: any, name: string, state_id: number) {
    this.id = id;
    this.name = name;
    this.state_id = state_id;
  }
}
