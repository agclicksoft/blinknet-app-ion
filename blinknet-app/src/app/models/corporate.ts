import { BrandModel } from "./brand";
import { ProductCategoryModel } from "./product-category";
import { BranchOfActivityModel } from "./branch-of-activity";
import { UtilProvider } from "../../providers/util/util";
import { User } from "./user2";
import { Category } from "./category";
import { Address } from "./address";

export class CorporateModel {
  id: number;
  name: string;
  brands: BrandModel[];
  product_categories: ProductCategoryModel[];
  branch_of_activities: BranchOfActivityModel[];

  public static GetNewInstance(): CorporateModel {
    return new CorporateModel(null, null);
  }

  public static ParseFromObject(object): CorporateModel {
    const model = CorporateModel.GetNewInstance();
    const util = new UtilProvider();

    if (!object) return model;

    model.id = object.id;
    model.name = object.name;

    if (util.isSet(object.brands) && Array.isArray(object.brands)) {
      model.brands = object.brands.map(o => BrandModel.ParseFromObject(o));
    }

    if (
      util.isSet(object.product_categories) &&
      Array.isArray(object.product_categories)
    ) {
      model.product_categories = object.product_categories.map(o =>
        ProductCategoryModel.ParseFromObject(o)
      );
    }

    if (
      util.isSet(object.branch_of_activities) &&
      Array.isArray(object.branch_of_activities)
    ) {
      model.branch_of_activities = object.branch_of_activities.map(o =>
        BranchOfActivityModel.ParseFromObject(o)
      );
    } else {
      model.branch_of_activities = [];
    }

    console.log("Model profile", model);

    return model;
  }

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
    this.brands = [];
    this.product_categories = [];
    this.branch_of_activities = [];
  }
}

export class Corporate {
  id: number;
  user: User;
  brands: any;
  operatingRegions: any = [];
  categories: Array<any> = [new Category];
  adresses: Array<Address> = [];
}
