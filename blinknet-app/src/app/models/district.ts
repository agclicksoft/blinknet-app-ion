export class DistrictModel {
  id: string;
  name: string;
  city_id: number;

  public static GetNewInstance(): DistrictModel {
    return new DistrictModel(null, null, null);
  }

  public static ParseFromObject(object): DistrictModel {
    const model = DistrictModel.GetNewInstance();

    if (object) {
      model.id = object.id;
      model.name = object.name;
      model.city_id = object.state_id;
    }

    return model;
  }

  constructor(id: any, name: string, state_id: number) {
    this.id = id;
    this.name = name;
    this.city_id = state_id;
  }
}
