import { Product } from "./product";
import { Buyer2 } from "./buyer2";

export class OrderProduct {
    id: number;
    product_id: number;
    buyer_id: number;
    created_at: string;
    updated_at: string;
    product : Product;
    buyer: Buyer2;
}