import { Product } from "./product";

export class Pagination {
    lastPage: number;
    page: number;
    perPage: number;
    total: number;
    data : Array<Product>;
}