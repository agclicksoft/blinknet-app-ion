import { ProviderModel } from "./provider";
import { BuyerModel } from "./buyer";
import { AddressModel } from "./address";
import { UtilProvider } from "../../providers/util/util";
import { CorporateModel } from "./corporate";

export class PersonModel {
  id: number;
  user_id: number;
  company_name: string;
  cnpj: string;
  latitude?: string;
  longitude?: string;
  telephone?: string;
  house_number?: string;
  role: string;
  provider?: ProviderModel;
  buyer?: BuyerModel;
  corporate?: CorporateModel;
  addresses: AddressModel[];

  public static GetNewInstance(): PersonModel {
    return new PersonModel(null, null, null, null, null, null);
  }

  public static ParseFromObject(object): PersonModel {
    let util = new UtilProvider();
    const model = PersonModel.GetNewInstance();

    if (!object) return model;

    model.id = object.id;
    model.user_id = object.user_id;
    model.company_name = object.company_name;
    model.cnpj = object.cnpj;
    model.telephone = object.telephone;
    model.house_number = object.house_number;
    model.longitude = object.longitude;
    model.latitude = object.latitude;
    model.provider = ProviderModel.ParseFromObject(object.provider);
    model.buyer = BuyerModel.ParseFromObject(object.buyer);
    model.corporate = CorporateModel.ParseFromObject(object.corporate);
    model.role = object.role;

    if (util.isSet(object.addresses) && Array.isArray(object.addresses)) {
      model.addresses = object.addresses.map(o =>
        AddressModel.ParseFromObject(o)
      );
    } else {
      model.addresses = [];
    }

    // console.log("person ParseFromObject object", object);
    // console.log("person ParseFromObject model ", model);

    return model;
  }

  constructor(
    id: number,
    company_name: string,
    cnpj: string,
    telephone: string,
    house_number: string,
    address_id: string
  ) {
    this.id = id;
    this.company_name = company_name;
    this.cnpj = cnpj;
    this.telephone = telephone;
    this.house_number = house_number;
    this.provider = ProviderModel.GetNewInstance();
    this.buyer = BuyerModel.GetNewInstance();
    this.corporate = CorporateModel.GetNewInstance();
    this.addresses = [];
    this.latitude = null;
    this.longitude = null;
    this.role = null;
  }
}
