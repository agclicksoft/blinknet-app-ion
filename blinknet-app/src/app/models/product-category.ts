export class ProductCategoryModel {
  id: number;
  name: string;

  public static GetNewInstance(): ProductCategoryModel {
    return new ProductCategoryModel(null, null);
  }

  public static ParseFromObject(object): ProductCategoryModel {
    const model = ProductCategoryModel.GetNewInstance();

    if (object) {
      model.id = object.id;
      model.name = object.name;
    }

    return model;
  }

  constructor(email: string, id: number) {
    this.id = id;
    this.name = name;
  }
}
