import { Provider2 } from "./provider2";
import { Category } from "./category";

export class Product {
    id: number;
    image_id: number;
    provider_id: number;
    adress_id: number;
    publication_text: string
    receive_orders: true;
    created_at: string;
    updated_at: string;
    image: Object;
    provider: Provider2;
    categories: Array<Category> = [new Category()];
}