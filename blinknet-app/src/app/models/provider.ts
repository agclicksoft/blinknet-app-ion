import { BrandModel } from "./brand";
import { ProductCategoryModel } from "./product-category";
import { UtilProvider } from "../../providers/util/util";

export class ProviderModel {
  id: number;
  name: string;
  brands: BrandModel[];
  product_categories: ProductCategoryModel[];

  public static GetNewInstance(): ProviderModel {
    return new ProviderModel(null, null);
  }

  public static ParseFromObject(object): ProviderModel {
    const model = ProviderModel.GetNewInstance();
    const util = new UtilProvider();

    if (!object) return model;

    model.id = object.id;
    model.name = object.name; 

    if (util.isSet(object.brands) && Array.isArray(object.brands)) {
      model.brands = object.brands.map(o => BrandModel.ParseFromObject(o));
    }

    if (
      util.isSet(object.product_categories) &&
      Array.isArray(object.product_categories)
    ) {
      model.product_categories = object.product_categories.map(o =>
        ProductCategoryModel.ParseFromObject(o)
      );
    }

    return model;
  }

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
    this.brands = [];
    this.product_categories = [];
  }
}
