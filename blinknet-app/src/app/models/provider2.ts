import { User } from "./user2";
import { Category } from "./category";
import { Address } from "./address";

export class Provider2 {
    id: number;
    user: User;
    brands: any;
    operatingRegions: any = [];
    categories: Array<any> = [new Category];
    adresses: Array<any> = [];
}