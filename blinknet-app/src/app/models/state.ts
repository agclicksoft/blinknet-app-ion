export class StateModel {
  id: string;
  name: string;
  initials: string;

  public static GetNewInstance(): StateModel {
    return new StateModel(null, null, null);
  }

  public static ParseFromObject(object): StateModel {
    const model = StateModel.GetNewInstance();

    if (object) {
      model.id = object.id;
      model.name = object.name;
      model.initials = object.initials;
    }

    return model;
  }

  constructor(id: any, name: string, initials: string ) {
    this.id = id;
    this.name = name;
    this.initials = initials;
  }
}
