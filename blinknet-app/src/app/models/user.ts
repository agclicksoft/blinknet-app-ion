import { PersonModel } from "./person";
import { UtilProvider } from "../../providers/util/util";

export class UserModel {
  id: number;
  name: string;
  email: string;
  cel: string;
  password?: string;
  token?: string;
  person_provider?: PersonModel;
  person_buyer?: PersonModel;
  person_corporate?: PersonModel;
  avatar_base64: string;
  role: string;
  type: string;

  public static GetNewInstance(): UserModel {
    return new UserModel(null, null, null, null, null, null);
  }

  public static ParseFromObject(object): UserModel {
    const util = new UtilProvider();
    const model = UserModel.GetNewInstance();

    if (!object) return model;

    model.id = object.id;
    model.name = object.name;
    model.email = object.email;
    model.cel = object.cel;
    model.token = object.token;
    model.password = object.password;
    model.avatar_base64 = object.avatar_base64;

    if (util.isSet(object.Addresses)) {
      if (util.isSet(object.person_buyer)) {
        object.person_buyer.address = object.Addresses;
      }

      if (util.isSet(object.person_provider)) {
        object.person_provider.address = object.Addresses;
      }

      if (util.isSet(object.person_corporate)) {
        object.person_corporate.address = object.Addresses;
      }
    }

    if (util.isSet(object.person_provider)) {
      model.person_provider = PersonModel.ParseFromObject(object.person_provider);
    } else {
      model.person_provider = PersonModel.GetNewInstance();
    }

    if (util.isSet(object.person_buyer)) {
      model.person_buyer = PersonModel.ParseFromObject(object.person_buyer);
    } else {
      model.person_buyer = PersonModel.GetNewInstance();
    }

    if (util.isSet(object.person_corporate)) {
      model.person_corporate = PersonModel.ParseFromObject(object.person_corporate);
    } else {
      model.person_corporate = PersonModel.GetNewInstance();
    }

    console.log("XXXX", model);

    // console.log("user ParseFromObject object", object);
    // console.log("user ParseFromObject model ", model);

    return model;
  }

  constructor(
    email: string,
    id: number,
    token: string,
    cel: string,
    name: string,
    avatar_base64: string
  ) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.cel = cel;
    this.token = token;
    this.avatar_base64 = avatar_base64;
   // this.person_seller = PersonModel.GetNewInstance();
    //this.person_buyer = PersonModel.GetNewInstance();
    //this.person_corporate = PersonModel.GetNewInstance();
  }
}
