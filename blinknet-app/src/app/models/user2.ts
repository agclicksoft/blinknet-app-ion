import { List } from "ionic-angular";
import { Provider2 } from "./provider2";
import { Buyer2 } from "./buyer2";
import { Image } from "./image";
import { Corporate } from "./corporate";
import { Storage } from "@ionic/storage";

export class User {
    id: number;
    email: string;
    password: string;
    name: string;
    cel: string;
    cnpj: null;
    company_name: string;
    created_at: string;
    updated_at: string;
    adress_id: number;
    image_id: number;
    usersRoles: List;
    buyer: Buyer2 = new Buyer2();
    corporate: Corporate = new Corporate();
    provider: Provider2 = new Provider2;
    image: Image;
    currente_role: string;
    role: string;
}
