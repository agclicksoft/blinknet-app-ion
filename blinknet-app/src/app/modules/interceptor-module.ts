import { Injectable, NgModule } from "@angular/core";
import { Observable } from "rxjs/Observable";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders
} from "@angular/common/http";
import { HTTP_INTERCEPTORS } from "@angular/common/http";

import { Storage } from "@ionic/storage";
import { UtilProvider } from "../../providers/util/util";


@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  token: string;


   constructor(
    public storage: Storage,
    public utilProvider: UtilProvider
  ) {
    this.storage.get('token').then(
      token => {this.token = token}
    )
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    if (
      req.url.includes("viacep") ||
      req.url.includes('google') ||
      req.url.includes('products?page') ||
      this.token === null
    ) 
      {
      return next.handle(req);
    }

    let headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`
    });

    const customRequest = req.clone({
      headers: headers
    });

    return next.handle(customRequest);
  }
}

@NgModule({
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpsRequestInterceptor,
      multi: true
    }
  ]
})
export class InterceptorModule { }
