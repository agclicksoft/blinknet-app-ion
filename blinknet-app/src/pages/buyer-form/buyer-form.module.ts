import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyerFormPage } from './buyer-form';
import { DirectivesModule } from "../../directives/directives.module";
import { BrMaskerModule } from 'brmasker-ionic-3';


@NgModule({
  declarations: [
    BuyerFormPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyerFormPage),
    DirectivesModule,
    BrMaskerModule
  ],
})
export class BuyerFormPageModule {}
