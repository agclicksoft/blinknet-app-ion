import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ToastController, normalizeURL } from "ionic-angular";
import { Storage } from "@ionic/storage";

import { UserProvider } from "../../providers/user/user";
import { AuthProvider } from "../../providers/auth/auth";
import { UtilProvider } from "../../providers/util/util";
import { AddressProvider } from "../../providers/address/address";
import { LoadingProvider } from "../../providers/loading/loading";
import { CommonProvider } from "../../providers/common/common";
import { CityModel } from "../../app/models/city";
import { DistrictModel } from "../../app/models/district";
import { User } from "../../app/models/user2";

@IonicPage()
@Component({
  selector: "page-buyer-form",
  templateUrl: "buyer-form.html"
})
export class BuyerFormPage {

  user: User = new User();
  branchOfActivities: any[] = [];
  form: FormGroup;
  avatarBase64: string = null;
  displayImageName: string = null;
  displayImageSrc: string = null;
  latitude: string = null;
  longitude: string = null;
  states: any[] = [];
  cities: CityModel[] = [];
  districts: DistrictModel[] = [];
  isInserting: boolean = true;
  address: any = {};
  districtId: number = null;
  cityId: number = null;

  options: CameraOptions = {
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private userProvider: UserProvider,
    private toastCtrl: ToastController,
    private authProvider: AuthProvider,
    private utilProvider: UtilProvider,
    public loadingCtrl: LoadingProvider,
    public addressProvider: AddressProvider,
    private storage: Storage,
    private commonProvider: CommonProvider
  ) {

    this.storage.get('user').then( response => {
      this.user = JSON.parse(response)
      const user = this.navParams.get('data');
     // this.user.password = user.password;
      this.user.buyer.adresses = new User().buyer.adresses;
    })

    this.displayImageSrc = null;

    if (!this.isInserting) {
      this.form = this.formBuilder.group({
        companyName: ["", Validators.compose([Validators.required])],
        cnpj: ["", Validators.compose([])],
        telephone: ["", Validators.compose([])],
        houseNumber: ["", Validators.compose([])],
        complement: ["", Validators.compose([])],
        cep: ["", Validators.compose([Validators.required])],
        street: ["", Validators.compose([Validators.required])],
        district: ["", Validators.compose([Validators.required])],
        city: ["", Validators.compose([Validators.required])],
        state: ["", Validators.compose([Validators.required])],
        branch_of_activities: [[], Validators.compose([Validators.required])],
        number_of_boxes: ["", Validators.compose([])]
      });
    } 
    else {
      this.form = this.formBuilder.group({
        companyName: ["", Validators.compose([Validators.required])],
        cep: ["", Validators.compose([Validators.required])],
        branch_of_activities: [[], Validators.compose([Validators.required])]
      });
    }
  }

  ionViewDidLoad() {
    this.fetchStates();
    this.getBranchOfActivities();
  }

  getBranchOfActivities() {
    this.userProvider.getBranchOfActivities().then(
      response => { this.branchOfActivities = response }     
    ).catch(
      error => { }
    )
  }

   onFormSubmit() {
    if (!this.form.valid) return;

    if (!this.user.buyer.adresses[0].street) {
      this.presentToast("CEP inválido.");
      return;
    }

    this.user.buyer.categories = this.form.value.branch_of_activities
      .map( branch => ( { name: branch } ))

    this.user.company_name = this.form.value.companyName;

    this.commonProvider.updateUser(this.user, 'buyers', this.user.buyer.id).then(
      async response => {
        this.loadingCtrl.present();        
        await this.storage.set('user', JSON.stringify(this.user)),
        this.navCtrl.setRoot(
          "SideMenuPage", {}, { animate: true, direction: "forward" }
        )
      }
    ).catch(
      error => {
        console.log(error)
      })
  }

  onRegisterSuccess(user) {
    this.authProvider.signIn(user.email, user.password).subscribe(
      async response => {
        await this.storage.set('token', response.data.token),
        await this.storage.set('user', JSON.stringify(this.user)),
        this.authProvider.user = this.user;
        this.navCtrl.setRoot(
          "SideMenuPage", {}, { animate: true, direction: "forward" }
        )
      },
      error => {
        this.presentToast("Erro ao efetuar login.");
      }
    )
  }

  onCepBlur(event) {
    const cep = this.form.value.cep;

    if (!this.utilProvider.isSet(cep) || cep.length < 5) {
      return;
    }

    this.loadingCtrl.present();
    let cepSanitazed = cep.replace(/\D/g, "");

    this.address = {};

    this.userProvider
      .getCepData(cepSanitazed)
      .then((response: any) => {
        this.loadingCtrl.dismiss();
        this.handleCepResponse(response);
      })
      .catch(error => {
        this.address = [];
        this.loadingCtrl.dismiss();
        console.log("onCepBlur", error);
      });

    this.fetchGeolocation(cepSanitazed);
  }

  async handleCepResponse(response) {

    if (response.erro) {
      this.address = [];
      this.presentToast("CEP inválido.");
      return;
    }

    this.loadingCtrl.present();

    this.user.buyer.adresses = [{
      zip_code: response.cep,
      state: response.uf,
      city: response.localidade,
      neighborhood: response.bairro,
      street: response.logradouro,
      number: '.',
      complement: '.'
    }]

    this.loadingCtrl.dismiss();
  }

  push(page) {
    this.navCtrl.setRoot(page, {}, { animate: true, direction: "forward" });
  }

  selectImage() {
    document.getElementById('uploadImage').click();
  }

  uploadImage(event) {
    this.loadingCtrl.present();
    const imageUpload = new FormData();
    const image = event.target.files[0];
    imageUpload.append('image', image);
    this.commonProvider.updateImageProfile(imageUpload, this.user.id).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error)
      }
    )
  
    const imageReader = new FileReader();
    imageReader.readAsDataURL(image);
    imageReader.onload = () => {
      this.displayImageSrc = imageReader.result as string;
    }
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  getIdForStateName(stateName) {
    if (this.states === undefined && this.states === null) return null;

    for (let state of this.states) {
      if (stateName == state.name) return state.id;
    }

    return null;
  }

  fetchStates() {
    console.log("getStates");

    this.addressProvider
      .getStates()
      .then((result: any) => {
        this.states = result.data;
      })
      .catch(error => console.log("error", error));
  }

  fetchCities(state_id: number = null) {
    this.addressProvider
      .getCities(state_id)
      .then((result: any) => {
        this.cities = result.data;
      })
      .catch(error => console.warn("getCities error", error));
  }

  fetchDistricts(city_id: number = null) {
    this.addressProvider
      .getDistricts(city_id)
      .then((result: any) => {
        this.districts = result.data;
      })
      .catch(error => console.warn("getCities error", error));
  }

  async fetchCity(cityId: number = null, name: string = null) {
    return await this.addressProvider.getCity(cityId, name);
  }

  async fetchDistrict(districtId: number = null, name: string = null) {
    return await this.addressProvider.getDistrict(districtId, name);
  }

  fetchGeolocation(param: string) {
    this.addressProvider
      .getLatLong(param)
      .then((response: any) => {
        if (response.results.length > 0) {
          this.latitude = response.results[0].geometry.location.lat;
          this.longitude = response.results[0].geometry.location.lng;
        }
      })
      .catch(error => {
        console.warn("onCepBlur", error);
      });
  }
}
