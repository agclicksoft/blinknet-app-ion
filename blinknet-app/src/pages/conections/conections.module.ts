import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConectionsPage } from './conections';

@NgModule({
  declarations: [
    ConectionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ConectionsPage),
  ],
})

export class ConectionsPageModule {}
