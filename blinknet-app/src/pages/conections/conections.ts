import { Component, ViewChild, ElementRef } from "@angular/core";
import {Platform, IonicPage, NavController, NavParams, ToastController } from "ionic-angular";
import { MenuController } from "ionic-angular";
import { SocialSharing } from "@ionic-native/social-sharing";
import { CallNumber } from "@ionic-native/call-number";
import { Geolocation } from "@ionic-native/geolocation";

import { AuthProvider } from "../../providers/auth/auth";
import { UserProvider } from "../../providers/user/user";
import { AuthenticatedUser } from "../../app/models/authenticated.user";
import { LoadingProvider } from "../../providers/loading/loading";
import { Storage } from "@ionic/storage";
import { User } from "../../app/models/user2";

declare var google;

@IonicPage()
@Component({
  selector: "page-conections",
  templateUrl: "conections.html"
})
export class ConectionsPage {
  @ViewChild("map")
  mapElement: ElementRef;
  map: any;
  mapIsLoaded: boolean = false;
  currentSegmentPage: string = "list";
  authUser: AuthenticatedUser;
  pendingSolicitations = [];
  pendingConnections = [];
  approvedConnections = [];
  connections = [];
  user: User = new User();
  currentVision: string;
  emptyConnections: string = '';

  constructor(
    public geolocation: Geolocation,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    public menuCtrl: MenuController,
    private platform: Platform,
    private authProvider: AuthProvider,
    private socialSharing: SocialSharing,
    private callNumber: CallNumber,
    public loadingCtrl: LoadingProvider,
    private toastCtrl: ToastController,
    private storage: Storage
  ) {
    this.authProvider.initAuth();
    this.pendingSolicitations = this.userProvider.pendingSolicitations;
    this.pendingConnections = this.userProvider.pendingConnections;
    this.approvedConnections = this.userProvider.approvedConnections;
  }

  ionViewDidEnter() {
    this.user = this.authProvider.user;
    console.log(this.user);
    this.currentVision = this.user.currente_role;
    this.fetchConnections();
  }

  async fetchConnections() {
    //TODO: Buscar conexões disponiveis
    // if (!this.authUser) return;
    this.loadingCtrl.present();
    const user = this.authProvider.user;
    this.userProvider.getConnections(this.user.currente_role,this.user[this.currentVision].id).then(
      data => {               
        this.pendingConnections =data.filter((connection) => {
          return connection.accept == false
        })      
        this.approvedConnections = data.filter((connection) => {
          return connection.accept == true
        })
        this.emptyConnections = 'Nenhuma conexão encontrada.'
        this.loadingCtrl.dismiss();
      },
      error=> {
        console.log(error);
        this.loadingCtrl.dismiss();
      }
    )
  }

  getName(connection) {
    if (connection.user !== undefined) return connection.user.name
    return connection[this.getRoleInvert(this.user.currente_role)].user.name    
  }
  getCompanyName(connection) {
    if (connection.user !== undefined) return connection.user.company_name
    return connection[this.getRoleInvert(this.user.currente_role)].user.company_name
  }
  getListItemImgSrc(connection) {
    
    let src = "assets/imgs/img_avatar.png";
  
    const imagem = connection[this.getRoleInvert(this.user.currente_role)].user.image  
    
    if (imagem) {
      src = imagem.path;
    }
    return src;
  }

  getRoleInvert(role) {
    if (role == 'provider') return 'buyer'
    if (role == 'buyer') return 'provider'
  }


  loadMap() {
    this.geolocation.getCurrentPosition().then(
      position => {
        let latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions
        );

        const markPosition = { lat: -22.852711, lng: -43.492396 };
        this.addMarker(markPosition, "john Doe", "Restaurante X");
      },
      err => {
        console.error(err);
      }
    );
  }

  onSegmentChange(event) {
    this.currentSegmentPage = event.value;

    if (event.value === "map" && !this.mapIsLoaded) {
      this.loadMap();
    }
  }

  accept(idConnection) {
    this.loadingCtrl.present();
    this.userProvider
      .accept(idConnection)
      .then(response => {
        console.log("updateConnectionStatus", response);
        this.loadingCtrl.dismiss();
        this.fetchConnections();
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        console.warn("updateConnectionStatus ", error);
      });
  }
  reject(idConnection) {
    this.loadingCtrl.present();
    this.userProvider
      .reject(idConnection)
      .then(response => {
        console.log("delete", response);
        this.loadingCtrl.dismiss();
        this.fetchConnections();
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        console.warn("updateConnectionStatus ", error);
      });
  }

  openWhatsApp(phoneNumber) {
    console.log("shareViaWhatsApp");

    if (this.platform.is("ios")) {
      const url = `https://api.whatsapp.com/send?phone=55${phoneNumber}`;
      window.open(url, "_system");
    } else {
      this.socialSharing
        .shareViaWhatsAppToReceiver(phoneNumber, null, null, null)
        .then(result => {
          console.log("shareViaWhatsAppToReceiver", result);
        })
        .catch(error =>
          console.warn("shareViaWhatsAppToReceiver error", error)
        );
    }
  }

  openEmail(email) {
    this.socialSharing
      .canShareViaEmail()
      .then(() => {
        this.socialSharing.shareViaEmail(
          "Venha conhecer o Blinknet!",
          "Blinknet",
          [email]
        );
      })
      .catch(error => {
        console.warn("canShareViaEmail error", error);
        this.presentToast(
          "O app não está habilitado a abrir a janela de compartilhamento"
        );
      });
  }

  openShareSheet() {

    // this.socialSharing.shareViaFacebook(
    //   "Você já conhece o novo aplicativo que conecta vendedores e compradores de todo Brasil? Baixe para Android gratuitamente!",
    //   "https://facebookbrand.com/wp-content/uploads/2019/04/f_logo_RGB-Hex-Blue_512.png?w=512&h=512",
    //   "https://play.google.com/store/apps/details?id=br.com.clicksoft.blinknet"
    // );

    const msg =
      "Você já conhece o novo aplicativo que conecta vendedores e compradores de todo Brasil? Baixe para Android gratuitamente!";
    this.socialSharing.share(
      msg,
      "Blinknet",
      null,
      "https://play.google.com/store/apps/details?id=br.com.clicksoft.blinknet"
    );
    //"http://blink.whmserver.net/api/users/redirectToAppStore"
  }

  callPhoneNumber(phoneNumber) {
    console.log("callPhoneNumber");
    this.callNumber
      .callNumber(phoneNumber, true)
      .then(res => console.log("Launched dialer!", res))
      .catch(err => console.log("Error launching dialer", err));
  }

  onClickUser(userId) {
    // TODO: abrir perfil do usuário
    console.log("onClickUser", userId);

    // if (this.approvedConnections.some(o => o.id === userId)) {
    //   this.navCtrl.push("ViewProfilePage", { userId: userId });
    // }
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, "click", () => {
      infoWindow.open(this.map, marker);
    });
  }

  addMarker(position, name, companyName) {
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      //position: this.map.getCenter()
      position: position
    });

    let content = `<h4>${name}</h4>`;
    content += `<p>${companyName}</p>`;
    content += `<div>
    <img src="assets/icon/envelope.png" width="20" style=" margin-right: 20px;" (click)="openEmail('1@1.com')"
    />
      <img item-end src="assets/icon/phone2-copy.png" width="20" (click)="callPhoneNumber('21 99572-6096')">
    </div>`;

    this.addInfoWindow(marker, content);
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }
}
