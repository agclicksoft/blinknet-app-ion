import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CorporateFormPage } from './corporate-form';
import { RlTagInputModule } from 'angular2-tag-input';
import { DirectivesModule } from '../../directives/directives.module';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    CorporateFormPage,
  ],
  imports: [
    IonicPageModule.forChild(CorporateFormPage),
    RlTagInputModule,
    DirectivesModule,
    BrMaskerModule
  ],
})
export class CorporateFormPageModule {}

