import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ToastController, normalizeURL, ModalController } from "ionic-angular";
import { Storage } from "@ionic/storage";

import { UserProvider } from "../../providers/user/user";
import { AuthProvider } from "../../providers/auth/auth";
import { UtilProvider } from "../../providers/util/util";
import { LoadingProvider } from "../../providers/loading/loading";
import { AddressProvider } from "../../providers/address/address";
import { CommonProvider } from "../../providers/common/common";
import { User } from "../../app/models/user2";
import { AddressModel } from "../../app/models/address";
import { DistrictModel } from "../../app/models/district";
import { CityModel } from "../../app/models/city";

@IonicPage()
@Component({
  selector: 'page-corporate-form',
  templateUrl: 'corporate-form.html',
})
export class CorporateFormPage {
  user: User = new User();
  categories = [];
  actingRegion = [];
  form: FormGroup;
  avatarBase64: string;
  displayImageSrc: string = null;
  displayImageName: string = null;
  regions: string[] = [];
  addresses: AddressModel[] = [];
  isInserting: boolean = true;
  cities: CityModel[] = [];
  districts: DistrictModel[] = [];
  latitude: string = null;
  longitude: string = null;
  branchOfActivities: any[] = [];
  
  options: CameraOptions = {
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingProvider,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private userProvider: UserProvider,
    private toastCtrl: ToastController,
    private authProvider: AuthProvider,
    private addressProvider: AddressProvider,
    private utilProvider: UtilProvider,
    private storage: Storage,
    private commonProvider: CommonProvider
  ) {
    this.initForm();
    this.storage.get('user').then( response => {
      this.user = JSON.parse(response)
      const user = this.navParams.get('data');
      this.user.password = user.password;
      this.user.corporate.adresses = new User().corporate.adresses;
    } )
    
    


  }

  ionViewDidLoad() {}

  ionViewDidEnter() {
    this.fetchStates();
    this.fetchCities();
    this.fetchBranchOfActivities();
  }

  onFormSubmit() {
    if (!this.form.valid) return;

    this.loadingCtrl.present();

    this.user.corporate.categories = this.form.value.product_categories
      .map(name => ( { name: name } ));

    this.user.corporate.brands = this.form.value.brands
      .map(name => ({ name: name } ));

    this.commonProvider.updateUser(this.user, 'corporates', this.user.corporate.id).then(
      response => {
        this.loadingCtrl.dismiss();
        this.onRegisterSuccess(this.user);
      }
    ).catch(
      error => {
        console.log(error)
      })
  }

  onRegisterSuccess(user) {
    this.loadingCtrl.present();

    this.authProvider.signIn(user.email, user.password).subscribe(
      async response => {
        await this.storage.set('token', response.data.token),
        await this.storage.set('user', JSON.stringify(this.user)),
        this.authProvider.user = this.user;
        this.loadingCtrl.dismiss();
        this.navCtrl.setRoot(
          "SideMenuPage", {}, { animate: true, direction: "forward" }
        )
      },
      error => {
        this.loadingCtrl.dismiss();
        this.presentToast("Erro ao efetuar login.");
      }
    )
  }

  initForm() {
    this.form = this.formBuilder.group({
      companyName: ["", Validators.compose([])],
      brands: [[], Validators.compose([Validators.required])],
      product_categories: [[], Validators.compose([Validators.required])]
    });
  }

  fetchGeolocation(param: string) {
    this.addressProvider
      .getLatLong(param)
      .then((response: any) => {
        if (response.results.length > 0) {
          this.latitude = response.results[0].geometry.location.lat;
          this.longitude = response.results[0].geometry.location.lng;
        }
      })
      .catch(error => {
        console.warn("onCepBlur", error);
      });
  }

  fetchCities(state_id: number = null) {
    this.addressProvider
      .getCities(state_id)
      .then((result: any) => {
        this.cities = result.data;
      })
      .catch(error => console.warn("getCities error", error));
  }

  fetchDistricts(city_id: number = null) {
    this.addressProvider
      .getDistricts(city_id)
      .then((result: any) => {
        this.districts = result.data;
      })
      .catch(error => console.warn("getCities error", error));
  }

  fetchStates() {
    this.addressProvider
      .getStates()
      .then((result: any) => {
        const stateNames = result.data.map(o => o.name);
        this.regions = this.regions.concat(stateNames);
      })
      .catch(error => console.error("error", error));
  }

  onDismissModal(data) {
    
    if (!data || !data.state || !data.city || !data.district) return

    if (!this.user.corporate.adresses)
      this.user.corporate.adresses = [];

    let address = {
      parents: "Brasil",
      state: data.state.name,
      city: data.city.name,
      neighborhood: data.district.name
    }
    this.user.corporate.adresses.push(address);
  }

  addOccupationArea() {
    const modal = this.modalCtrl.create("SelectAddressModalPage");
    modal.onDidDismiss(data => this.onDismissModal(data));
    modal.present();
  }

  removeOccupationArea(address) {
    const newAddresses = this.addresses.filter(
      o => o.zip_code !== address.zip_code
    );
    this.addresses = newAddresses;
  }

  selectImage() {
    document.getElementById('uploadImage').click();
  }

  uploadImage(event) {
    const imageUpload = new FormData();
    const image = event.target.files[0];
    imageUpload.append('image', image);
    this.commonProvider.updateImageProfile(imageUpload, this.user.id).subscribe(
      response => {
        console.log(response)
      },
      error => {
        console.log(error)
      }
    )

    const imageReader = new FileReader();
    imageReader.readAsDataURL(image);
    imageReader.onload = () => {
      this.displayImageSrc = imageReader.result as string;
    }
  }

  push(page) {
    this.navCtrl.setRoot(page, {}, { animate: true, direction: "forward" });
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  fetchBranchOfActivities() {
    this.userProvider.getBranchOfActivities().then(
      response => { this.branchOfActivities = response },
     
    ).catch(
      error => { console.log(error) }
    )
  }
}
  