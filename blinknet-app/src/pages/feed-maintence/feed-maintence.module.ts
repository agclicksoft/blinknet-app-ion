import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedMaintencePage } from './feed-maintence';

@NgModule({
  declarations: [
    FeedMaintencePage,
  ],
  imports: [
    IonicPageModule.forChild(FeedMaintencePage),
  ],
  exports: [
    FeedMaintencePage
  ]
})
export class FeedMaintencePageModule {}
