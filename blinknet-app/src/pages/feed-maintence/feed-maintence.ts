import { FeedProvider } from './../../providers/feed/feed';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, App, Form } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Camera, CameraOptions, EncodingType } from "@ionic-native/camera";
import { ToastController, normalizeURL } from "ionic-angular";


import { AuthProvider } from "../../providers/auth/auth";
import { UtilProvider } from "../../providers/util/util";
import { LoadingProvider } from "../../providers/loading/loading";

import { AddressProvider } from '../../providers/address/address';
import { CityModel } from '../../app/models/city';
import { DistrictModel } from '../../app/models/district';
import { Cep } from '../../app/models/cep';

@IonicPage()
@Component({
  selector: 'page-feed-maintence',
  templateUrl: 'feed-maintence.html',
})
export class FeedMaintencePage {
  form: FormGroup;
  avatarBase64: string = null;
  id: string = null;
  displayImageName: string = null;
  displayImageSrc: string = null;
  text: string = "";
  currentVision: string = "";
  states: any[] = [];
  cities: CityModel[] = [];
  districts: DistrictModel[] = [];
  receiveRequest: string = "";
  options: CameraOptions = {
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };
  cep: string
  viaCep: Cep
  image:any
  img_profile: FormData = new FormData()
  uploadForm: FormData
  error: any

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private feedProvider: FeedProvider,
    private toastCtrl: ToastController,
    private authProvider: AuthProvider,
    private utilProvider: UtilProvider,
    public loadingCtrl: LoadingProvider,
    public addressProvider: AddressProvider,
    public app: App) {
    this.viaCep = new Cep()
    this.currentVision = this.authProvider.getCurrentVision();
    this.displayImageSrc = null;

    this.form = this.formBuilder.group({
      textPublish: ["", Validators.compose([Validators.required])],
      state: ["", Validators.compose([Validators.required])],
      receiveRequest: [false],
    });


    let feedEdit = navParams.get('feed');

    if (feedEdit) {
      this.id = feedEdit.id;
      this.displayImageName = "edit";
      this.avatarBase64 = feedEdit.picture;
      this.displayImageSrc = feedEdit.picture;
      this.text = feedEdit.textpublish;
      this.form.value.textPublish = feedEdit.textpublish;
      this.form.value.receiveRequest = feedEdit.receiveRequest;
    }
  }

  ionViewDidLoad() {
    this.fetchStates();
  }

  onFormSubmit() {
    const params = {
      id: null,
      publication_text: this.form.value.textPublish,
      receive_orders: this.form.value.receiveRequest,
      adress: {
        parents: 'Brasil',
        state: this.viaCep.uf,
        city: this.viaCep.localidade,
        neighborhood: this.viaCep.bairro
      }
    };

    if (this.id) {
      params.id = parseInt(this.id, 10);
    }

    console.log("params", params);
    this.registerFeed(params);
  }

  registerFeed(feed: any) {
    this.loadingCtrl.present();

    this.feedProvider
      .add(feed)
      .then((result: any) => {

        this.feedProvider.uploadImg(this.uploadForm, result.data.id)
          .then((data) => {
            this.presentToast("Upload de foto concluido");
          })
          .catch(error => {
            this.error =error
            this.presentToast(`Error ao publicar com foto.`)
          })
         
        this.loadingCtrl.dismiss();
        this.onRegisterSuccess();
      })
      .catch(error => {
        console.log(error);
        this.loadingCtrl.dismiss();
        this.presentToast("Error ao publicar.");
      });
  }

  onRegisterSuccess() {
    this.presentToast("Publicação realizada com sucesso.");
    this.app.getActiveNav().pop();
  }


  showImagePicker(): void {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 900,
      targetHeight: 600,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false,
      allowEdit: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    this.camera.getPicture(options).then(
      imageUri => {
        const fileName = imageUri.substr(imageUri.lastIndexOf("/") + 1);
        const uri = normalizeURL(imageUri);

        this.utilProvider.resizeImage(500, uri).then((img: string) => {
          this.displayImageSrc = img;
          this.avatarBase64 = img;
          this.displayImageName = fileName;
        });
        this.displayImageSrc = 'data:image/jpeg;base64,' + imageUri;
       
        
        this.image = imageUri;
      },
      error => {
        console.warn("showImagePicker error", error);
      }
    );
  }

  // showImagePicker(): void {
  //   this.camera.getPicture(this.options).then(
  //     imageData => {
  //       this.displayImageSrc = 'data:image/jpeg;base64,' + imageData;;
  //       this.avatarBase64 = 'data:image/jpeg;base64,' + imageData;;
  //       this.displayImageName = "imagefeed";
  //     },
  //     error => {
  //       console.warn("showImagePicker error", error);
  //     }
  //   );
  // }
  teste() {
    document.getElementById('image').click()
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  delete() {
    this.loadingCtrl.present();

    this.feedProvider
      .delete(this.id)
      .then((result: any) => {
        console.log(result);
        this.loadingCtrl.dismiss();

        if (result.error) {
          this.presentToast("Error ao excluir publicação.");
        } else {
          this.presentToast("Publicação excluída com sucesso.");
          this.app.getActiveNav().pop();
        }
      })
      .catch(error => {
        console.log(error);
        this.loadingCtrl.dismiss();
        this.presentToast("Error ao excluir publicação.");
      });
  }

  onSelectChange(type) {
    console.log('onSelectChangetype', type);

  }

  async searchCEP(): Promise<any> {
    this.feedProvider.findAdress(this.cep)
      .then((data: Cep) => this.viaCep = data)
  }

  fetchCities(state_id: number = null) {
    this.addressProvider
      .getCities(state_id)
      .then((result: any) => {
        this.cities = result.data;
      })
      .catch(error => console.warn("getCities error", error));
  }

  fetchDistricts(city_id: number = null) {
    this.addressProvider
      .getDistricts(city_id)
      .then((result: any) => {
        this.districts = result.data;
      })
      .catch(error => console.warn("getCities error", error));
  }

  fetchStates() {
    console.log("getStates");

    this.addressProvider
      .getStates()
      .then((result: any) => {
        this.states = result.data;
      })
      .catch(error => console.log("error", error));
  }

  upload(event:any)
  {
    
    this.uploadForm = new FormData();
    const element = event.target.files[0];
    console.log(element);
    this.uploadForm.append('image', element);
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.displayImageSrc= reader.result as string;
    }   
  }

}
