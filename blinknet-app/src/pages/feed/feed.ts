import { FeedMaintencePage } from './../feed-maintence/feed-maintence';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ToastController, MenuController, Nav, ModalController, AlertController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import { AuthProvider } from '../../providers/auth/auth';
import { FeedProvider } from '../../providers/feed/feed';
import { UserProvider } from '../../providers/user/user';
import { AuthenticatedUser } from '../../app/models/authenticated.user';
import { RequestsPage } from './../requests/requests';
import { User } from '../../app/models/user2';



@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})



export class FeedPage {

  feeds: any = [];
  message: string = "";
  showMessage: boolean = false;
  authUser: AuthenticatedUser = new AuthenticatedUser("", 0, "", "");
  isCorporate: boolean = false;
  pageNumber: number = 1;
  currentVision: string;
  @ViewChild(Nav)
  nav: Nav;
  user: User;

  pendingSolicitations = [];
  pendingConnections = [];
  approvedConnections = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private feedProvider: FeedProvider,
    private userProvider: UserProvider,
    private toastCtrl: ToastController,
    private authProvider: AuthProvider,
    public loadingCtrl: LoadingProvider,
    public app: App,
    public menu: MenuController,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController
  ) {
    this.authProvider.initAuth();
  }

  ionViewDidLoad() {
    //this.menu.enable(true);
    this.message = "Carregando novidades...";
    this.showMessage = true;
    this.pageNumber = 1;
  }

  ionViewDidEnter() {
    this.user = this.authProvider.user;
    this.currentVision = this.user.currente_role;
    this.isCorporate = (this.currentVision == "corporate");
    this.fetchConnections(false);
  }

  async fetchConnections(incrementPage) {
    this.loadingCtrl.present();
    this.feedProvider.getAll(this.pageNumber).then(
      response => {
        this.feeds = response;
        if(incrementPage)
          this.pageNumber++;
        this.loadingCtrl.dismiss();
      }
    ).catch(
      error => {
        console.log(error)
        this.loadingCtrl.dismiss();
      }
    )
    this.showMessage = false;
  }

  fetchMoreConnections(page, event) {
    this.pageNumber++;
    this.feedProvider.getAll(this.pageNumber).then(
      response => {
        let feeds: any = response
            feeds = feeds.data
        for (let feed of feeds)
          this.feeds.data.push(feed);
        event.complete();
        this.loadingCtrl.dismiss();
      },
      error => {
        this.loadingCtrl.dismiss();
      }
    )
  }

  updateName(avatar, feed) {
    //metodo para diminuir o nome
    let name;
    if (avatar) {
      name = (!feed.person || feed.person[0].type != 'corporate') ? feed.user.name : feed.person[0].company_name;
    } else {
      name = (!feed.person || feed.person[0].type != 'corporate') ? feed.user.name : feed.person[0].company_name;
    }

    if (name.length > length) {
      name = name.substring(0, 3) + "...";
    }
    return name;
  }

  doRefresh(refresher) {
    this.message = "";
    this.showMessage = false;
    this.pageNumber = 1;
    this.fetchConnections(false);
    refresher.complete();
  }

  addFeed() {
    this.app.getActiveNav().push(FeedMaintencePage);
  }

  getFeeds(onCompleted: any, resetFeeds: boolean) {
    this.pageNumber = (resetFeeds) ? 1 : this.pageNumber;
    //TODO : Fazer nova requiisição a lista de feeds
    // // this.feedProvider.getAll(this.pageNumber)
    // // .then((result: any) => {
    // //   if(result.data.length == 0 && this.pageNumber == 1)
    // //     {
    // //       this.pageNumber = this.pageNumber;
    // //     } else if (result.data.length == 0 && this.pageNumber > 1){
    // //       console.log('página '+this.pageNumber+' vazia')
    // //       this.pageNumber = this.pageNumber - 1;
    // //       console.log(this.pageNumber);
    // //     }
    // //   console.log(result);
    // //   if (onCompleted) {
    // //     onCompleted();
    // //   }
    // //   this.loadingCtrl.dismiss();
    // //   if (result.error) {
    // //     this.showMessage = false;
    // //     this.message = "";
    // //     this.presentToast("Erro ao carregar novidades.");
    // //   } else {

    // //     this.onRegisterSuccess(result, resetFeeds);
    // //   }
    // // })
    // // .catch(error => {
    // //   this.showMessage = false;
    // //   this.message = "";
    // //   if (onCompleted) {
    // //     onCompleted();
    // //   }
    //   console.log(error);
    //   this.loadingCtrl.dismiss();
    //   this.presentToast("Houve um erro e não foi possível carregar as novidades. " + error);
    // });
  }

  onRegisterSuccess(result, resetFeeds) {
    this.showMessage = false;
    this.message = "";

    let feedsProcessed = []

    for (let index = 0; index < result.data.length; index++) {
      const element = result.data[index];

      element.allowConnect = false;
      element.allowPending = false;

      if (this.isCorporate ||
        element.user.id == this.authUser.id ||
        element.person[0].type == 'corporate' ||
        this.isInArray(element.user.id, this.approvedConnections)) {
        element.displayConnectionStatus = false;
      } else {
        element.displayConnectionStatus = true;
        if (this.isInArray(element.user.id, this.pendingSolicitations)) {
          element.allowPending = true;
          element.userConnection = this.getInArray(element.user.id, this.pendingSolicitations);
        } else {
          element.allowConnect = true;
        }
      }

      feedsProcessed.push(element);
    }

    if (resetFeeds) {
      this.feeds.data = [];
    }

    this.feeds.data = this.feeds.data.concat(feedsProcessed);

    if (this.feeds.data.length == 0) {
      this.message = "Nenhuma novidade encontrada.";
      this.showMessage = true;
    }
  }

  isInArray(value, myArray) {
    let result = false;

    for (let index = 0; index < myArray.length; index++) {
      const element = myArray[index];

      if (value == element.id) {
        result = true;
        break;
      }
    }

    return result;
  }

  getInArray(value, myArray) {
    let result = null;

    for (let index = 0; index < myArray.length; index++) {
      const element = myArray[index];

      if (value == element.id) {
        result = element;
        break;
      }
    }

    return result;
  }

  getFriendRole() {
    if (this.currentVision == 'provider') return 'buyer'
    if (this.currentVision == 'buyer') return 'provider'
  }

  openForm(productId) {
    let alert = this.alertCtrl.create({
      title: 'Informações adicionais',
      inputs: [
        {
          name: 'amount',
          placeholder: 'Quantidade'
        },      
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Efetuar pedido',
          handler: data => {
            this.addOrderRequest(data.amount, productId)
          }
        }
      ]
    });
    alert.present();
  }

  addOrderRequest(amount, productId) {
    this.loadingCtrl.present();
    this.userProvider
      .addOrderRequest(amount, productId)
      .then((response: any) => {
        this.presentToast("Seu pedido foi enviado!");
        this.loadingCtrl.dismiss();
        this.fetchConnections(true)
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        this.presentToast("Ocorreu um erro, tente novamente.");
        console.warn("addConnectionRequest error", error);
      });
  }

  addConnectionRequest(providerId, amount, productId) {

    this.loadingCtrl.present();
    const friendId = providerId
    this.userProvider
      .addConnectionRequest(providerId , friendId, this.getFriendRole(), amount, productId)
      .then((response: any) => {
        this.presentToast("Seu pedido de conexão foi enviado!");
        this.loadingCtrl.dismiss();
        this.fetchConnections(true)
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        this.presentToast("Ocorreu um erro, tente novamente.");
        console.warn("addConnectionRequest error", error);
      });
  }

  onPendentStatusPress(feed) {

    // if (!feed.userConnection) {
    //   console.log("Ação Pendente foi abortada.");
    //   return;
    // }

    // let startUpdate = false;
    // const user = feed.userConnection;

    // console.log("User pendente", user);

    // if (
    //   user.parent_users &&
    //   user.parent_users.length > 0 &&
    //   user.parent_users[0].id === this.authUser.id
    // ) {
    //   status = user.parent_users[0]._joinData.status;
    //   const userId = user.child_users[0]._joinData.user_id;
    //   const friendId = user.child_users[0]._joinData.friend_id;
    //   startUpdate = true;
    //   this.updateConnectionStatus(userId, friendId, 0);
    // } else if (
    //   user.child_users &&
    //   user.child_users.length > 0 &&
    //   user.child_users[0].id === this.authUser.id
    // ) {
    //   const userId = user.child_users[0]._joinData.user_id;
    //   const friendId = user.child_users[0]._joinData.friend_id;
    //   startUpdate = true;
    //   this.updateConnectionStatus(userId, friendId, 0);
    // }

    // feed.allowConnect = true;
    // feed.allowPending = false;

    // this.feeds = this.feeds.map(o => {
    //   if (o.user.id === user.id) {
    //     o.connectionStatus = 0;
    //   }

    //   return o;
    // });

    // if (!startUpdate) {
    //   this.fetchConnections(true);
    // }
  }

  updateConnectionStatus(userId, friendId, status) {
    this.userProvider
      .updateConnectionStatus(userId, friendId, status)
      .then(response => {
        this.fetchConnections(true);
      })
      .catch(error => {
        console.log("updateConnectionStatus", error);
      });
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  openViewProfile(userId: any) {
    this.navCtrl.push("ViewProfilePage", { userId: userId });

    // if (this.approvedConnections.some(o => o.id === userId)) {
    //   this.navCtrl.push("ViewProfilePage", { userId: userId });
    // }
  }

  doInfinite(event) {
    if (this.pageNumber <= this.feeds.lastPage)
      this.fetchMoreConnections(this.pageNumber, event);

    if (this.pageNumber > this.feeds.lastPage){
      event.disabled = true;
    }
      
    
    // infiniteScroll.complete();

    // if (this.pageNumber == this.feeds.lastPage)
    //   infiniteScroll.target.disabled = true;
    // this.getFeeds(() => { infiniteScroll.complete(); }, false);
  }

  openFeed() {
    this.navCtrl.setRoot(RequestsPage);
  }
}
