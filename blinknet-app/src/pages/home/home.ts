import { Component } from "@angular/core";
import { NavController, IonicPage, MenuController, LoadingController } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";

import { AuthProvider } from "../../providers/auth/auth";
import { UserProvider } from "../../providers/user/user";

@IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  loadingSpinner: any;

  constructor(
    public navCtrl: NavController,
    public authProvider: AuthProvider,
    public userProvider: UserProvider,
    private statusBar: StatusBar,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
  ) {
    this.statusBar.styleLightContent();
    this.checkIfUserIsLoggedIn();
  }
  ionViewDidEnter(){
   //this.authProvider.setCurrentVision(null, null);
  }
  presentLoadingSpinner() {
    this.loadingSpinner = this.loadingCtrl.create({
      content: "Por favor aguarde..."
    });

    this.loadingSpinner.present();
  }

  dismissLoadingSpinner() {
    //this.loadingSpinner.dismiss();
  }
  async checkIfUserIsLoggedIn() {
    const isAuthenticated = this.authProvider.user;
    
    if (isAuthenticated) {
      this.dismissLoadingSpinner();
      this.navCtrl.setRoot("SideMenuPage", {}, { animate: true, direction: "forward" }
      );
    } 
    else {
      console.log("User is not authenticated");
    }

    console.log("this.authProvider.isAuthenticated()", isAuthenticated);
  }

  push(page: string) {
    this.navCtrl.push(page);
  }

  async fetchConnections(authUser: any, userType: string) {

    // Usuario corporativo nao se conecta ao ninguem
    // if (userType === "corporate") {
    //   return;
    // }

    // const fetchUserWithType = userType === "provider" ? "buyer" : "provider";


    //await this.userProvider
      // .getConnections(authUser.id, fetchUserWithType)
      // .then((response: any) => {

      //   if ("error" in response) {
      //     throw String(response.error);
      //   } else {
      //     this.userProvider.pendingConnections = response.data.pending.filter(
      //       o => o._joinData.user_id !== authUser.id
      //     );

      //     this.userProvider.pendingSolicitations = response.data.pending.filter(
      //       o => o._joinData.user_id === authUser.id
      //     );

      //     this.userProvider.approvedConnections = response.data.approved.filter(
      //       o => o.id != authUser.id
      //     );
      //   }
      // })
      // .catch(error => {
      //   console.warn("getConnections error", error);
      // });
  }


}
