import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { AlertController, LoadingController, ToastController } from "ionic-angular";

import { AuthProvider } from "../../providers/auth/auth";
import { UserProvider } from "../../providers/user/user";
import { LoadingProvider } from "../../providers/loading/loading";


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  credentialsForm: FormGroup;
  loadingSpinner: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AuthProvider,
    public userProvider: UserProvider,
    public loadingCtrl: LoadingProvider,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private storage: Storage,
  ) {
    this.credentialsForm = this.formBuilder.group({
      email: ["", Validators.compose([Validators.email, Validators.required])],
      password: [
        "",
        Validators.compose([Validators.minLength(3), Validators.required])
      ]
    });
  }

  ionViewDidLoad() {
    this.storage.get('user').then(
      response => console.log(response)
    )
  }

  onSignIn() {
    if (this.credentialsForm.valid) {
      // this.presentLoadingSpinner();
      this.loadingCtrl.present()
      const email = this.credentialsForm.value.email;
      const password = this.credentialsForm.value.password;

      this.authProvider.signIn(email, password).subscribe(
        async response => {
          this.loadingCtrl.dismiss();

          for (let property in response.user) 
            if (response.user[property] == null) 
              delete response.user[property];
          
          await this.storage.set('token', response.data.token);
          await this.storage.set('user', JSON.stringify(response.user))

          this.authProvider.user = response.user;
          this.authProvider.currentVision = response.user.currente_role;
          this.navCtrl.setRoot( "SideMenuPage", {}, { animate: true, direction: "forward" } );
        },
        error => {
          this.presentAlert("Erro", "E-mail ou senha incorreto(s)");
          this.loadingCtrl.dismiss();
        }
      )
    } 
    else {
      this.presentToast("Preencha o e-mail corretamente.");
    }
  }

  onForgotPassword() {
    console.log("SignInPage: onForgotPassword()");

    let prompt = this.alertCtrl.create({
      title: "Recuperar senha",
      inputs: [
        {
          name: "email",
          placeholder: "E-mail",
          type: "email"
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Recuperar",
          handler: data => {
            let validateObj = this.validateEmail(data.email);

            if (validateObj.isValid) {
              this.recuperatePassword(data.email);
            } else {
              prompt.setMessage("Preencha o e-mail corretamente.");
              return false;
            }
          }
        }
      ]
    });

    prompt.present();
  }

  push(page) {
    switch (page) {
      case "register":
        this.navCtrl.push("RegisterPage");
        break;
    }
  }

  recuperatePassword(email) {
    this.authProvider
      .recuperatePassword(email)
      .then(result => {
        this.presentAlert(
          "Sucesso",
          "Uma nova senha foi enviada para seu email!"
        );
      })
      .catch(error => {
        this.presentAlert("Error", "Tente novamente mais tarde.");
      });
  }

  presentAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ["Ok"]
    });

    alert.present();
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  // presentLoadingSpinner() {
  //   this.loadingSpinner = this.loadingCtrl.create({
  //     content: "Por favor aguarde..."
  //   });

  //   this.loadingSpinner.present();
  // }

  validateEmail(email) {
    if (/(.+)@(.+){2,}\.(.+){2,}/.test(email)) {
      return {
        isValid: true,
        message: ""
      };
    } else {
      return {
        isValid: false,
        message: "Email address is required"
      };
    }
  }
}
