import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { RlTagInputModule } from 'angular2-tag-input/dist';
import { DirectivesModule } from "../../directives/directives.module";
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    RlTagInputModule,
    DirectivesModule,
    BrMaskerModule
  ],
})
export class ProfilePageModule {}
