import { FeedProvider } from './../../providers/feed/feed';
import { Component, ɵConsole } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  normalizeURL,
  ModalController,
  AlertController,
  App,
  MenuController
} from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { Storage } from '@ionic/storage';

import { AuthenticatedUser } from "../../app/models/authenticated.user";
import { AuthProvider } from "../../providers/auth/auth";
import { UserProvider } from "../../providers/user/user";
import { StateProvider } from "../../providers/state/state";
import { UtilProvider } from "../../providers/util/util";
import { LoadingProvider } from "../../providers/loading/loading";
import { CommonProvider } from '../../providers/common/common';
import { User } from "../../app/models/user2";
import { Buyer2 } from '../../app/models/buyer2';
import { AddressModel } from "../../app/models/address";
import { FeedMaintencePage } from '../feed-maintence/feed-maintence';

@IonicPage()
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  // authUser: AuthenticatedUser;

  userModel: User = new User;
  formModel: User = new User;
  currentVision: string = "provider";
  buttonText: string = "";
  buyerAddress: AddressModel;
  buyerCategories: any = [];
  corporateAddress: any = {};
  corporateCategories: Array<string> = [];
  corporateBrands: Array<string> = [];
  providerCategories: Array<string> = [];
  providerBrands: Array<string> = [];

  // form: FormGroup;
  
  profileImgSrc: string = "assets/imgs/placeholder.png";
  states: any[] = [];
  branchOfActivities: any[] = [];
  shouldShowNumOfBoxes: boolean = false;
  shouldLoadUserData: boolean = true;
  shouldDisplayRemovePicBtn: boolean = false;
  feeds: any = [];
  feedsPage: number = 0;
  returnMoreFeeds: boolean = true;

  

  options: CameraOptions = {
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private authProvider: AuthProvider,
    private userProvider: UserProvider,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingProvider,
    private stateProvider: StateProvider,
    private utilProvider: UtilProvider,
    private modalCtrl: ModalController,
    private feedProvider: FeedProvider,
    private app: App,
    public menu: MenuController,
    private storage: Storage,
    private commonProvider: CommonProvider
  ) {
    this.currentVision = this.authProvider.getCurrentVision();
    console.log(this.currentVision);
    // this.authUser = AuthenticatedUser.GetNewInstance();
    this.buyerAddress = AddressModel.GetNewInstance();

    // this.form = this.formBuilder.group({
    //   company_name: ["", Validators.compose([Validators.required])],
    //   cnpj: ["", Validators.compose([Validators.required])],
    //   occupation_areas: [[], Validators.compose([Validators.required])],
    //   brands: [[], Validators.compose([Validators.required])],
    //   product_categories: [[], Validators.compose([Validators.required])]
    // });
  }

  ionViewDidLoad() {
    // this.currentVision = 'provider'
    this.menu.enable(true);
    if (this.states.length === 0) this.fetchStates();
    this.fetchBranchOfActivities();
  }

  ionViewDidEnter() {
    if (this.shouldLoadUserData) {
      this.loadAuthUserData();
      this.shouldLoadUserData = false;
    }

    this.returnMoreFeeds = true;
    this.feeds = [];
    this.feedsPage = 0;

    setTimeout(() => {
      this.fetchFeeds();
    }, 200);
  }

  setButtonText() {
    if (this.currentVision === "provider") {
      this.formModel.buyer ? this.buttonText = "Trocar para comprador" 
        : this.buttonText = "Cadastrar como comprador";
    } 
    else {
      this.formModel.provider ? this.buttonText = "Trocar para vendedor" 
        : this.buttonText = "Cadastrar como vendedor";
    }
  }

  onButtonPress() {

    if (this.currentVision === "provider") {
      this.currentVision = "buyer";
      this.authProvider.setCurrentVision("buyer", this.userModel.id);
      if (!this.formModel.buyer) 
        this.formModel.buyer = new User().buyer;
    }
    else {
      this.currentVision = "provider";
      this.authProvider.setCurrentVision("provider", this.userModel.id);
      if (!this.formModel.provider) 
        this.formModel.provider = new User().provider; 
    }

    this.setButtonText();
  }

  async loadAuthUserData() {

    this.loadingCtrl.present();

    let user = await this.storage.get('user');
        user = JSON.parse(user);

    this.formModel = this.userModel = user;

    this.setFormModel();

    this.loadingCtrl.dismiss();
  }

  setFormModel() {

      // CONFIGURANDO FORMULÁRIO CASO USUÁRIO TENHA PERFIL DE VENDEDOR
      if ( this.userModel.provider ) {

        if (this.userModel.provider.categories && this.userModel.provider.categories.length > 0) {
          this.providerCategories = this.userModel.provider.categories
            .map(category => category.name);
        }
  
        if (this.userModel.provider.adresses && this.userModel.provider.adresses.length > 0) {
          let regions = [];
  
          for (let i in this.userModel.provider.adresses)
            regions.push(this.userModel.provider.adresses[i]);
  
          this.formModel.provider.adresses = regions
            .map( region => ( {
              parents: region.parents,
              state: region.state,
              city: region.city,
              neighborhood: region.neighborhood
            } ))
        }
  
        if (this.userModel.provider.brands && this.userModel.provider.brands.length > 0)
          this.providerBrands = this.userModel.provider.brands.map(
            brand =>  brand.name 
          );
  
        if (this.userModel.provider.categories && this.userModel.provider.categories.length > 0) {
          this.formModel.provider.categories = this.userModel.provider.categories.map(
            category => category.name
          )
        }   
      }

    // CONFIFURANDO FORMULÁRIO CASO USUÁRIO TENHA PERFIL DE COMPRADOR
    if ( this.currentVision === 'buyer' ) {

      if (!this.userModel.buyer.adresses)
        this.userModel.buyer.adresses = new User().buyer.adresses;

      // if (this.userModel.buyer.adresses) {
      //   this.buyerAddress.zip_code = this.userModel.buyer.adresses[0].zip_code;
      //   this.buyerAddress.street = this.userModel.buyer.adresses[0].street;
      //   this.buyerAddress.number = this.userModel.buyer.adresses[0].number;
      //   this.buyerAddress.complement = this.userModel.buyer.adresses[0].complement;
      //   this.buyerAddress.district.name = this.userModel.buyer.adresses[0].neighborhood;
      //   this.buyerAddress.city.name = this.userModel.buyer.adresses[0].city;
      //   this.buyerAddress.state.name = this.userModel.buyer.adresses[0].state;
      // }
        
      if (!this.userModel.buyer.categories)
        this.userModel.buyer.categories = new User().buyer.categories;

      if (this.userModel.buyer.categories && this.userModel.buyer.categories.length > 0)
        this.buyerCategories = this.userModel.buyer.categories
          .map(category =>  category.name )
    }

    // CONFIGURANDO FORMULÁRIO CASO USUÁRIO TENHA PERFIL CORPORATIVO
    if ( this.userModel.corporate ) {

      if (this.userModel.corporate.adresses.length > 0 && this.userModel.corporate.adresses[0].zip_code)
        this.corporateAddress = this.userModel.corporate.adresses[0];

      if (this.userModel.corporate.categories.length > 0)
        this.corporateCategories = this.userModel.corporate.categories
        .map( category =>  category.name )

      if (this.userModel.corporate.brands.length > 0)
        this.corporateBrands = this.userModel.corporate.brands
          .map( brand => brand.name )
    }

    this.onCepBlur(null);
    this.setButtonText();
  }

  selectImage() {
    document.getElementById('uploadImage').click();
  }

  uploadImage(event) {
    const uploadImage = new FormData();
    const image = event.target.files[0];
    console.log(image);
    uploadImage.append('image', image);
    this.commonProvider.updateImageProfile(uploadImage, this.userModel.id).subscribe(
      response => {
        console.log(response)
      },
      error => {
        console.log(error)
      }
    )

    const imageReader = new FileReader();
    imageReader.readAsDataURL(image);
    imageReader.onload = () => this.profileImgSrc = imageReader.result as string;
  }

  onFormSubmit() {

    this.loadingCtrl.present();

    let user = this.formModel;

    delete user.password;

    // ENVIANDO FORMULÁRIO CASO USUÁRIO TENHA PERFIL DE VENDEDOR
    if (this.currentVision === 'provider') {
      user.provider.adresses = this.formModel.provider.adresses
        .map( region => ( { 
          state: region.state,
          city: region.city,
          neighborhood: region.neighborhood
        } ));

      user.provider.brands = this.providerBrands
        .map( brand => ( {name: brand} ) );

      user.provider.categories = this.providerCategories
        .map( name => ( { name: name } ));
    }

    // ENVIANDO FORMULÁRIO CASO USUÁRIO TENHA PERFIL DE COMPRADOR
    if (this.currentVision === 'buyer') {

      user.buyer.adresses = [{
        state: this.buyerAddress.state.name,
        city: this.buyerAddress.city.name,
        neighborhood: this.buyerAddress.district.name,
        zip_code: this.buyerAddress.zip_code,
        street: this.buyerAddress.street,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        number: this.buyerAddress.number,
        complement: this.buyerAddress.complement
      }]

      user.buyer.categories = this.buyerCategories
        .map( category => ( { name: category } ))
    }

    // ENVIANDO FORMULÁRIO CASO USUÁRIO TENHA PERFIL CORPORATIVO
    if (this.currentVision === 'corporate') {
      user.corporate.brands = this.corporateBrands
        .map( name => ( { name: name } ));

      user.corporate.categories = this.corporateCategories
        .map( name => ( { name: name } ) );

      user.corporate.adresses.unshift({
        zip_code: this.corporateAddress.zip_code,
        street: this.corporateAddress.street,
        number: this.corporateAddress.number,
        complement: this.corporateAddress.complement,
        neighborhood: this.corporateAddress.neighborhood,
        city: this.corporateAddress.city,
        state: this.corporateAddress.state
      })
    }

    console.log(user);

    if (user.provider && user.provider.id && this.currentVision === 'provider' 
        || 
        user.buyer && user.buyer.id && this.currentVision === 'buyer'
      )      
      this.commonProvider.updateUser(user, `${this.currentVision}s`, user[this.currentVision].id).then(
        async response => {
          if (response.data){
            if (response.data.buyer && response.data.buyer.adresses && response.data.buyer.adresses.id)
              response.data.buyer.adresses = [response.data.buyer.adresses];
            await this.storage.set('user', JSON.stringify(response.data));
            this.authProvider.user = response.data;
          }

          if (response.user){
            if (response.user.buyer && response.user.buyer.adresses && response.user.buyer.adresses.id)
              response.user.buyer.adresses = [response.user.buyer.adresses];
            await this.storage.set('user', JSON.stringify(response.user));
            this.authProvider.user = response.user;
          }
          
          console.log(response);
          
          this.loadingCtrl.dismiss();
        })

    if (user.provider && !user.provider.id && this.currentVision === 'provider'
        ||
        user.buyer && !user.buyer.id && this.currentVision === 'buyer'
        ) 
        this.commonProvider.setRole(user, `${this.currentVision}s`).subscribe(
          async response => {

            if (response.user){
              if (response.user.buyer && response.user.buyer.adresses && response.user.buyer.adresses.id)
                response.user.buyer.adresses = [response.user.buyer.adresses]; 
              await this.storage.set('user', JSON.stringify(response.user));
              this.authProvider.user = response.user;
            }
             
            if (response.data){
              if (response.data.buyer && response.data.buyer.adresses && response.data.buyer.adresses.id) 
                response.data.buyer.adresses = [response.data.buyer.adresses];
              console.log(response.data);
              await this.storage.set('user', JSON.stringify(response.data));
              this.authProvider.user = response.data;
            }
                        
            this.loadingCtrl.dismiss();
          },
          error => {
            console.log(error);
            this.loadingCtrl.dismiss();
          }
        )

    if (this.currentVision === 'corporate') {
      this.commonProvider.updateUser(user, `${this.currentVision}s`, user[this.currentVision].id).then(
        async response => {
          if (response.data){
            await this.storage.set('user', JSON.stringify(response.data));
            this.authProvider.user = response.data;
          }

          if (response.user){
            await this.storage.set('user', JSON.stringify(response.user));
            this.authProvider.user = response.user;
          }
        })
      }
    this.setButtonText();
  }

  addOccupationArea() {
    const modal = this.modalCtrl.create("SelectAddressModalPage");
    modal.onDidDismiss(data => {
      this.onDismissModal(data)

      // if(this.currentVision == "provider"){
      //   this.formModel.provider.adresses.push(data);
      // } else if(this.currentVision == "corporate"){
      //   this.formModel.corporate.adresses.push(data);
      // }

    });
    modal.present();
  }

  removeOccupationArea(address) {

    if (this.currentVision === 'provider' && this.formModel.provider)
    for (let i in this.formModel.provider.adresses) {
      if (address.neighborhood === this.formModel.provider.adresses[i].neighborhood) {
        this.formModel.provider.adresses.splice(parseInt(i), 1);
      }
    }
    else if (this.currentVision === 'buyer' && this.formModel.buyer) {
      for (let i in this.formModel.buyer.adresses) {
        if (address.neighborhood === this.formModel.buyer.adresses[i].neighborhood) {
          this.formModel.buyer.adresses.splice(parseInt(i), 1);
        }
      }
    }
  }

  onDismissModal(data) {

    if (!data || !data.state || !data.city || !data.district) return

    let address = {
      state: data.state.name,
      city: data.city.name,
      neighborhood: data.district.name
    }

    if (this.currentVision === 'provider') {
      this.formModel.provider.adresses.push(address);
      console.log(this.formModel);
    } 
    else if (this.formModel.corporate) {
      this.formModel.corporate.adresses.push(address);
    }
    
  }

  onBranchChange(event) {
    // this.shouldShowNumOfBoxes = this.formModel.buyer.branch_of_activities.some(
    //   id => id === 37 || id === 38
    // );

    // if (!this.shouldShowNumOfBoxes)
    //   this.formModel.buyer.number_of_boxes = "";
  }

  onCepBlur(event) {

    let zipCode = this.buyerAddress.zip_code;

    if (!zipCode) {
      zipCode = this.corporateAddress.zip_code;
    }

    if (this.utilProvider.isSet(zipCode) && zipCode.length > 5) {
      let zipCodeSanitazed = zipCode.replace(/\D/g, "");

      this.userProvider
        .getCepData(zipCodeSanitazed)
        .then((result: any) => {

          if (this.buyerAddress) {
            this.buyerAddress.street = result.logradouro;
            this.buyerAddress.district.name = result.bairro;
            this.buyerAddress.city.name = result.localidade;

            const stateId = this.getIdForStateName(result.localidade);
            this.buyerAddress.state_id = stateId;
            this.buyerAddress.state.id = stateId;
            this.buyerAddress.state.name = result.localidade;
          }

          if (this.currentVision === 'corporate') {
            this.corporateAddress.street = result.logradouro;
            this.corporateAddress.neighborhood = result.bairro;
            this.corporateAddress.city = result.localidade;
            this.corporateAddress.state = result.localidade;
          }
        })
        .catch(error => {
          console.warn("onCepBlur", error);
        });
    }
  }

  onChangePassBtnPress() {
    this.presentChangePassPrompt();
  }

  onRemovePicPress() {
    this.profileImgSrc = "assets/imgs/placeholder.png";
    // this.formModel.avatar_base64 = null;
  }

  presentChangePassPrompt() {
    let alert = this.alertCtrl.create({
    title: 'Alterar senha',
    inputs: [
      {
        name: 'password',
        placeholder: 'Nova senha',
        type: 'password'
      },
      {
        name: 'password_2',
        placeholder: 'Repita a nova senha',
        type: 'password'
      }
    ],
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Alterar',
        handler: data => {
          if (data.password, data.password_2 && data.password === data.password_2) {
            this.updatePassword(this.userModel.id, data.password);
            console.log("valid");
            return true;
          } else {
            this.presentToast("As senhas não são iguais.");
            return false;
          }
        }
      }
    ]
    });
    alert.present();
  }

  updatePassword(userId: number, password: string) {
    this.userProvider.updatePassword(userId, password)
      .then((response: any) => {
        if (response.status === "error") throw "return status is not OK";
        this.presentToast("A senha foi alterada com sucesso!");
      })
      .catch(error => {
        this.presentToast("Erro ao alterar a senha.");
      });
  }

  getIdForStateName(stateName) {
    for (let state of this.states) {
      if (stateName == state.name) return state.id;
    }

    return null;
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  fetchStates() {
    this.stateProvider
      .getAll()
      .then((result: any) => {
        this.states = result.data;
        this.onCepBlur(null);
      })
      .catch(error => console.log("error", error));
  }

  fetchBranchOfActivities() {
    this.userProvider.getBranchOfActivities()
    .then(
      response => { this.branchOfActivities = response }     
    ).catch(
      error => console.log(error)
    )
  }

  fetchFeeds() {

    // console.log("TESTEEEE", this.authUser)

    // this.feedProvider
    //   .getByUserId(this.authUser.id, this.feedsPage + 1)
    //   .then((result: any) => {

    //     if (result.data.length < 4) {
    //       this.returnMoreFeeds = false;

    //       if (result.data.length == 0) {
    //         return;
    //       }
    //     }

    //     let insertItem1: boolean = true;

    //     for (let index = 0; index < result.data.length; index++) {

    //       if (insertItem1) {
    //         this.feeds.push({ item1: result.data[index], item2: null });
    //         insertItem1 = false;
    //       } else {
    //         this.feeds[this.feeds.length - 1].item2 = result.data[index];
    //         insertItem1 = true;
    //       }
    //     }

    //     this.feedsPage = this.feedsPage + 1;

    //   })
    //   .catch(error => console.log("error", error));
  }

  seeMore() {
    this.fetchFeeds();
  }

  showImagePicker() {
    this.camera.getPicture(this.options).then(
      imageUri => {
        console.log("showImagePicker got image");
        const uri = normalizeURL(imageUri);

        this.utilProvider.resizeImage(300, uri).then((img: string) => {
          this.profileImgSrc = img;
          // this.formModel.avatar_base64 = img;
          this.shouldDisplayRemovePicBtn = true;
        });
      },
      error => {
        console.warn("showImagePicker error", error);
      }
    );
  }

  editFeed(feed) {
    this.app.getActiveNav().push(FeedMaintencePage, {
      feed: feed
    });
  }
}
