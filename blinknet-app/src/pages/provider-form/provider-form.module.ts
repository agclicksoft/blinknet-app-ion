import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderFormPage } from './provider-form';
import { RlTagInputModule } from 'angular2-tag-input/dist';
import { DirectivesModule } from "../../directives/directives.module";
import { BrMaskerModule } from 'brmasker-ionic-3';


@NgModule({
  declarations: [
    ProviderFormPage,
  ],
  imports: [
    IonicPageModule.forChild(ProviderFormPage),
    RlTagInputModule,
    DirectivesModule,
    BrMaskerModule
  ],
})
export class ProviderFormPageModule {}
