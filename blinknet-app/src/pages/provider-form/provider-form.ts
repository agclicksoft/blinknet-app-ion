import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ToastController, normalizeURL, ModalController } from "ionic-angular";
import { FileTransfer } from "@ionic-native/file-transfer";
import { DomSanitizer } from "@angular/platform-browser";

import { UserProvider } from "../../providers/user/user";
import { AuthProvider } from "../../providers/auth/auth";
import { UtilProvider } from "../../providers/util/util";
import { AddressProvider } from "../../providers/address/address";
import { AddressModel } from "../../app/models/address";
import { LoadingProvider } from "../../providers/loading/loading";
import { CityModel } from "../../app/models/city";
import { DistrictModel } from "../../app/models/district";
import { CommonProvider } from "../../providers/common/common";
import { Storage } from '@ionic/storage';
import { User } from "../../app/models/user2";

@IonicPage()
@Component({
  selector: "page-provider-form",
  templateUrl: "provider-form.html"
})
export class ProviderFormPage {

  user: User = new User();
  categories = [];
  actingRegion = [];
  form: FormGroup;
  avatarBase64: string;
  displayImageSrc: string = null;
  displayImageName: string = null;
  regions: string[] = [];
  addresses: AddressModel[] = [];
  isInserting: boolean = true;
  states: any[] = [];
  cities: CityModel[] = [];
  districts: DistrictModel[] = [];
  latitude: string = null;
  longitude: string = null;
  districtId: number = null;
  cityId: number = null;
  providerCategories: Array<string> = [];
  branchOfActivities: any[] = [];

  options: CameraOptions = {
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingProvider,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private userProvider: UserProvider,
    private commonProvider: CommonProvider,
    private toastCtrl: ToastController,
    private authProvider: AuthProvider,
    private addressProvider: AddressProvider,
    private transfer: FileTransfer,
    private sanitizer: DomSanitizer,
    private utilProvider: UtilProvider,
    private storage: Storage
  ) {
    this.initForm();
    this.storage.get('user').then( response => {
      this.user = JSON.parse(response)
      const user = this.navParams.get('data');
   //   this.user.password = user.password;
      this.user.provider.adresses = new User().provider.adresses;
    })

  }

  ionViewDidLoad() {
    this.fetchBranchOfActivities();
  }

  ionViewDidEnter() {
    this.fetchStates();
    this.fetchCities();
  }

  initForm() {
    if (!this.isInserting) {
      this.form = this.formBuilder.group({
        companyName: ["", Validators.compose([])],
        cnpj: ["", Validators.compose([])],
        brands: ["", Validators.compose([])],
      });
    } else {
      this.form = this.formBuilder.group({
        companyName: ["", Validators.compose([])],
        brands: [[], Validators.compose([])],
      });
    }
  }

  async onFormSubmit() {
    if (!this.form.valid) return;

    this.loadingCtrl.present();

    this.user.provider.categories = this.providerCategories
      .map(name => ( { name: name } ));

    this.user.provider.brands = this.form.value.brands
      .map(name => ({ name: name }));

    this.user.company_name = this.form.value.companyName;
    this.user.cnpj = this.form.value.cnpj;
    
    const token = await this.storage.get('token')
    if (!token) {
      this.presentToast("Token não encontrado.");
    }
    
 
    this.commonProvider.updateUser(this.user, 'providers', this.user.provider.id).then(
      async response => {
        await this.storage.set('user', JSON.stringify(response.user))
        this.loadingCtrl.dismiss();     
        this.presentToast("Auto login efetuado com sucesso.");
        this.navCtrl.setRoot("SideMenuPage", {}, { animate: true, direction: "forward" })
      })
      .catch(error =>  this.presentToast("Erro ao atualizar perfil."))
  }

  getIdForStateName(stateName) {
    if (this.states === undefined && this.states === null) return null;

    for (let state of this.states) 
      if (stateName == state.name) 
        return state.id;

    return null;
  }

  onCepBlur(event) {
    const cep = this.form.value.cep;

    if (!this.utilProvider.isSet(cep) || cep.length < 5) {
      return;
    }

    this.loadingCtrl.present();
    let cepSanitazed = cep.replace(/\D/g, "");

    this.userProvider.getCepData(cepSanitazed).then((response: any) => {
        this.loadingCtrl.dismiss();
        this.handleCepResponse(response);
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        console.warn("onCepBlur", error);
      });

    this.fetchGeolocation(cepSanitazed);
  }

  fetchGeolocation(param: string) {
    this.addressProvider.getLatLong(param).then((response: any) => {
        if (response.results.length > 0) {
          this.latitude = response.results[0].geometry.location.lat;
          this.longitude = response.results[0].geometry.location.lng;
        }
      })
      .catch(error => {
        console.warn("onCepBlur", error);
      });
  }

  async handleCepResponse(response) {
    this.loadingCtrl.present();

    this.form.controls.street.setValue(response.logradouro);
    this.form.controls.district.setValue(response.bairro);
    this.form.controls.city.setValue(response.localidade);

    const stateId = this.getIdForStateName(response.localidade);
    this.form.controls.state.setValue(stateId);

    const district: any = await this.fetchDistrict(null, response.bairro);
    if (district && district.data && district.data.id) {
      this.districtId = district.data.id;
    }

    const city: any = await this.fetchCity(null, response.localidade);
    if (city && city.data && city.data.id) {
      this.cityId = city.data.id;
    }

    this.loadingCtrl.dismiss();
  }

  async fetchDistrict(districtId: number = null, name: string = null) {
    return await this.addressProvider.getDistrict(districtId, name);
  }

  async fetchCity(cityId: number = null, name: string = null) {
    return await this.addressProvider.getCity(cityId, name);
  }

  onRegisterSuccess(user) {
    this.loadingCtrl.present();

    this.authProvider.signIn(user.email, user.password).subscribe(
      async response => {
        this.loadingCtrl.dismiss();
        await this.storage.set('user', JSON.stringify(response.user));
        this.authProvider.user = response.user;
        await this.storage.set('token', response.data.token).then(
          response => this.navCtrl.setRoot("SideMenuPage", {}, { animate: true, direction: "forward" })
        ).catch(
          error => console.log(error)
        )
        
      },
      error => {
        this.loadingCtrl.dismiss();
        this.presentToast("Erro ao efetuar login.");
      }
    )
  }

  fetchStates() {
    this.addressProvider.getStates().then((result: any) => {
      const stateNames = result.data.map(o => o.name);
      this.regions = this.regions.concat(stateNames);
    })
    .catch(error => console.error("error", error));
  }

  fetchCities() {
    this.addressProvider.getStatesCities().then((result: any) => {
      result.states.forEach(state => {
        this.regions = this.regions.concat(state.name);
      });
    })
    .catch(error => console.error("error", error));
  }

  addOccupationArea() {
    const modal = this.modalCtrl.create("SelectAddressModalPage");
    modal.onDidDismiss(data => this.onDismissModal(data));
    modal.present();
  }

  removeOccupationArea(address) {
    const newAddresses = this.addresses.filter(
      o => o.zip_code !== address.zip_code
    );
    this.addresses = newAddresses;
  }

  onDismissModal(data) {
    
    if (!data || !data.state || !data.city || !data.district) return

    let address = {
      parents: "Brasil",
      state: data.state.name,
      city: data.city.name,
      neighborhood: data.district.name
    }
    this.user.provider.adresses.push(address);
  }

  selectImage() {
    document.getElementById('uploadImage').click();
  }

  uploadImage(event) {
    const imageUpload = new FormData();
    const image = event.target.files[0];
    imageUpload.append('image', image);
    this.commonProvider.updateImageProfile(imageUpload, this.user.id).subscribe(
      response => {
        console.log(response)
      },
      error => {
        console.log(error)
      }
    )

    const imageReader = new FileReader();
    imageReader.readAsDataURL(image);
    imageReader.onload = () => {
      this.displayImageSrc = imageReader.result as string;
    }
  }

  /*showImagePicker() {
    this.camera.getPicture(this.options).then(
      imageUri => {
        const fileName = imageUri.substr(imageUri.lastIndexOf("/") + 1);

        // let base64Image = "data:image/jpeg;base64," + imageData;
        const uri = normalizeURL(imageUri);
        //const uri = imageUri.replace('file://', '');
        //const uri = imageUri.replace(/^file:\/\//, '');
        //this.displayImageSrc = uri;

        console.log("showImagePicker displayImageSrc", uri);
        console.log("fileName", fileName);

        this.utilProvider.resizeImage(300, uri).then((img: string) => {
          this.displayImageSrc = img;
          this.avatarBase64 = img;
          this.displayImageName = fileName;
        });
      },
      error => {
        console.warn("showImagePicker error", error);
      }
    );
  }*/

  push(page) {
    this.navCtrl.setRoot(page, {}, { animate: true, direction: "forward" });
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  onBranchChange(event) {
    // this.shouldShowNumOfBoxes = this.formModel.buyer.branch_of_activities.some(
    //   id => id === 37 || id === 38
    // );

    // if (!this.shouldShowNumOfBoxes)
    //   this.formModel.buyer.number_of_boxes = "";
  }

  fetchBranchOfActivities() {
    this.userProvider.getBranchOfActivities().then(
      response => { this.branchOfActivities = response },
  
    ).catch(
      error => { console.log(error) }
    )
  }

}
