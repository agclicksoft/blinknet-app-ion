import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserModel } from "../../app/models/user";
import { PersonModel } from "../../app/models/person";

import { UserProvider } from "../../providers/user/user";
import { LoadingProvider } from "../../providers/loading/loading";
import { AuthProvider } from "../../providers/auth/auth";
import { Storage } from "@ionic/storage";
import { User } from "../../app/models/user2";

@IonicPage()
@Component({
  selector: "page-register",
  templateUrl: "register.html"
})
export class RegisterPage {

  registerForm: FormGroup;
  userModel: User = new User;

  constructor(
    private storage: Storage,
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingProvider,
    private userProvider: UserProvider,
    private authProvider: AuthProvider,
    private toastCtrl: ToastController
  ) {
    const emailPattern = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@[*[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+]*/;

    this.registerForm = this.formBuilder.group({
      email: [
        "",
        Validators.compose([
          Validators.pattern(emailPattern),
          Validators.required
        ])
      ],
      password: [
        "",
        Validators.compose([Validators.minLength(3), Validators.required])
      ],
      name: [
        "",
        Validators.compose([Validators.minLength(4), Validators.required])
      ],
      cel: [
        "",
        Validators.compose([Validators.minLength(8), Validators.required])
      ],
      role: ["provider"]
    });
  
  }

  ionViewDidLoad() {}

  setUserRole(role) {
    this.registerForm.controls["role"].setValue(role);
  }

  onFormSubmit() {
    this.loadingCtrl.present();

    if (!this.registerForm.valid) return;
    this.userModel.email = this.registerForm.value.email;
    this.userModel.name = this.registerForm.value.name;
    this.userModel.password = this.registerForm.value.password;
    this.userModel.cel = this.registerForm.value.cel;
    this.userModel.role = this.registerForm.value.role;

    if (this.registerForm.value.role === "provider") {
      this.userModel.provider = new User().provider;
      delete this.userModel.buyer;
      delete this.userModel.corporate;
    }
    else if (this.registerForm.value.role === "buyer") {
      this.userModel.buyer = new User().buyer;
      delete this.userModel.provider;
      delete this.userModel.corporate;
    } else {
      this.userModel.corporate = new User().corporate;
      delete this.userModel.provider;
      delete this.userModel.buyer;
    }

    this.authProvider.register(this.userModel).subscribe(
      response => {
        this.userModel.id = response.user.id;
        this.userModel[this.userModel.role].id = response.user[this.userModel.role].id;
        this.loadingCtrl.dismiss();
        this.afterSaveUser(response)
      },
      error => {
        console.log(error)
        this.loadingCtrl.dismiss();
        this.presentToast(error.error.data);
      }
    )
  }

  async afterSaveUser(response) {

    await this.storage.set("token", response.data.token );
    await this.storage.set('user', JSON.stringify(response.user));
    console.log(this.userModel);

    await this.authProvider.initCurrentVision();

    if (this.userModel.role === "provider") {
      this.navCtrl.push("ProviderFormPage", { data: this.userModel });
    }
    else if (this.userModel.role === "buyer") {
      this.navCtrl.push("BuyerFormPage", { data: this.userModel });
    }
    else if (this.userModel.role === "corporate") {
      this.navCtrl.push("CorporateFormPage", { data: this.userModel });
    }
  }


  changePage(page) {
    this.navCtrl.setRoot(page, {}, { animate: true, direction: "forward" });
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }
}
