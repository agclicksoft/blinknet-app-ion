import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Nav, Platform } from 'ionic-angular';
import { AuthenticatedUser } from '../../app/models/authenticated.user';
import { AuthProvider } from '../../providers/auth/auth';
import { FeedPage } from './../feed/feed';
import { UserProvider } from '../../providers/user/user';
import { LoadingProvider } from '../../providers/loading/loading';
import { RequestsProvider } from '../../providers/requests/requests';
import { Storage } from '@ionic/storage';
import { User } from '../../app/models/user2';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from "@ionic-native/call-number";

/**
 * Generated class for the RequestsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-requests',
  templateUrl: 'requests.html',
})
export class RequestsPage {
  authUser: User;
  currentVision: string;
  feeds: any = [];
  emptyMessage: string = '';
  
  @ViewChild(Nav)
  nav: Nav;

  message: string = "";
  showMessage: boolean = false;
  isCorporate: boolean = false;
  pageNumber: number = 1;
  pendingSolicitations = [];
  pendingConnections = [];
  approvedConnections = [];
  user : User

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authProvider: AuthProvider,
    private userProvider: UserProvider,
    public loadingCtrl: LoadingProvider,
    private requestsProvider: RequestsProvider,
    private toastCtrl: ToastController,
    private storage: Storage,
    private platform: Platform,
    private socialSharing: SocialSharing,
    private callNumber: CallNumber,

    ) {
      this.authUser = this.authProvider.user;
      const currentVision = this.authProvider.getCurrentVision();
      this.isCorporate = (currentVision == "corporate");
  }

  ionViewDidLoad() {
    this.fetchConnections(null);
    this.pageNumber= 1;
  }

  async fetchConnections(onComplete: any) {
    this.loadingCtrl.present();

    await this.storage.get('user')
      .then(user => {
        this.user = JSON.parse(user),
        this.currentVision = this.user.currente_role;
        console.log(this.user)
      })

    const role = this.user.currente_role + 's';
    const id = this.user[this.user.currente_role].id;
    const page = this.pageNumber;
    
    this.requestsProvider.getAll(role, id, page)
    .then(
      response => {
        this.feeds = response
        if (this.feeds.data.length === 0)
          this.emptyMessage = 'Nenhuma novidade encontrada';
        this.loadingCtrl.dismiss();
      }
    )
    .catch(
      error => {
        console.log(error)
      }
    )
    this.showMessage = false;
  }

  fetchMoreConnections(page, event) {

    const role = this.user.currente_role + 's';
    const id = this.user[this.user.currente_role].id;
    
    this.requestsProvider.getAll(role, id, page)
    .then(
      response => {
        for (let feed of response.data)
          this.feeds.data.push(feed);
        this.pageNumber++;
        event.complete();
      }
    )
    .catch(
      error => {
        console.log(error);
      }
    )
  }
  
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  onRegisterSuccess(result, resetFeeds) {
    this.showMessage = false;
    this.message = "";

    let feedsProcessed = []

    for (let index = 0; index < result.data.length; index++) {
      const element = result.data[index];

      element.allowConnect = false;
      element.allowPending = false;

      if (this.isCorporate ||
            element.user.id == this.authUser.id ||
            element.person[0].type == 'corporate' ||
            this.isInArray(element.user.id, this.approvedConnections)) {
        element.displayConnectionStatus = false;
        console.log("displayConnectionStatus false", element.id);
      } else {
        element.displayConnectionStatus = true;
        console.log("displayConnectionStatus true", element.id);
        if (this.isInArray(element.user.id, this.pendingSolicitations)) {
          element.allowPending = true;
          element.userConnection = this.getInArray(element.user.id, this.pendingSolicitations);
        } else {
          element.allowConnect = true;
        }
      }

      feedsProcessed.push(element);
    }

    if (resetFeeds) {
      this.feeds = [];
    }

    this.feeds = this.feeds.concat(feedsProcessed);

    console.log(this.feeds);

    if (this.feeds.length == 0) {
      this.message = "Nenhuma novidade encontrada.";
      this.showMessage = true;
    }
  }

  isInArray(value, myArray) {
    let result = false;

    for (let index = 0; index < myArray.length; index++) {
      const element = myArray[index];

      if (value == element.id) {
        result = true;
        break;
      }
    }

    return result;
  }

  getRoleRevert() {
    console.log(this.currentVision);
    if (this.currentVision == 'provider')  return 'buyer'
    if (this.currentVision == 'buyer')  return 'product.provider'
    if (this.currentVision == 'corporate') return 'buyer'
  }

  getInArray(value, myArray) {
    let result = null;

    for (let index = 0; index < myArray.length; index++) {
      const element = myArray[index];

      if (value == element.id) {
        result = element;
        break;
      }
    }

    return result;
  }

  openFeed() {
    this.navCtrl.setRoot(FeedPage);
  }

  contactEmail(email) {
    this.socialSharing
    .canShareViaEmail()
    .then(() => {
      this.socialSharing.shareViaEmail(
        "Venha conhecer o Blinknet!",
        "Blinknet",
        [email]
      );
    })
    .catch(error => {
      console.warn("canShareViaEmail error", error);
      this.presentToast(
        "O app não está habilitado a abrir a janela de compartilhamento"
      );
    });
  }

  contactPhone(phoneNumber) {
    console.log("callPhoneNumber");
    this.callNumber
      .callNumber(phoneNumber, true)
      .then(res => console.log("Launched dialer!", res))
      .catch(err => console.log("Error launching dialer", err));
  }

  contactWhatsApp(phoneNumber) {
    console.log("shareViaWhatsApp");

    if (this.platform.is("ios")) {
      const url = `https://api.whatsapp.com/send?phone=55${phoneNumber}`;
      window.open(url, "_system");
    } else {
      this.socialSharing
        .shareViaWhatsAppToReceiver(phoneNumber, null, null, null)
        .then(result => {
          console.log("shareViaWhatsAppToReceiver", result);
        })
        .catch(error =>
          console.warn("shareViaWhatsAppToReceiver error", error)
        );
    }
  }

  doInfinite(event) {
    if (this.pageNumber <= this.feeds.lastPage)
    this.fetchMoreConnections(this.pageNumber, event);

    console.log(this.pageNumber)
    console.log(this.feeds.lastPage);

    if (this.pageNumber > this.feeds.lastPage){
      event.disabled = true;
    }
  }

}
