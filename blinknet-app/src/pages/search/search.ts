import { PersonModel } from './../../app/models/person';
import { Component, ElementRef, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  ModalController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";

import { AuthProvider } from "../../providers/auth/auth";
import { UserProvider } from "../../providers/user/user";
import { AuthenticatedUser } from "../../app/models/authenticated.user";
import { UtilProvider } from "../../providers/util/util";
import { LoadingProvider } from "../../providers/loading/loading";
import { Storage } from '@ionic/storage';
import { User } from '../../app/models/user2';
import { FormBuilder } from '@angular/forms';

declare var google;

@IonicPage()
@Component({
  selector: "page-search",
  templateUrl: "search.html"
})
export class SearchPage {
  @ViewChild("map")
  mapElement: ElementRef;
  map: any;
  mapIsLoaded: boolean = false;
  currentSegmentPage: string = "list";
  shouldShowCancel: boolean = false;
  user: User;
  searchInput: string;
  searchInput2: string;
  locationInput: string;
  currentVision: string;
  users: any[];
  pendingConnections = [];
  approvedConnections = [];
  searchText1: string = "";
  searchText2: string = "";
  branchOfActivities: any[] = [];
  isCorporate: boolean = false;
  filter: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    public loadingCtrl: LoadingProvider,
    public utilProvider: UtilProvider,
    public geolocation: Geolocation,
    private authProvider: AuthProvider,
    private toastCtrl: ToastController,
    private storage: Storage,
    private fb: FormBuilder,
    private modalCtrl: ModalController
  ) {
 
    this.user = this.authProvider.user;
    this.currentVision = this.authProvider.getCurrentVision();
    this.fetchUsers();
    this.isCorporate = (this.currentVision == "corporate");
  
    console.log('isCorporate' + this.isCorporate);
  }

  formModel = this.fb.group({
    state: [''],
    city: [''],
    neighborhood: ['']
  })

  ionViewDidEnter() {
    this.currentSegmentPage = "list";

    this.setupFilterText();
    this.fetchUsers();
    this.fetchBranchOfActivities();   

  }

  ionViewWillLeave() {
    this.users = [];
    this.searchInput = "";
    this.searchInput2 = "";
    this.locationInput = "";
    this.loadingCtrl.dismiss();
  }

  onSearchTextChange(event) {
    if (this.currentVision === 'provider'){
      this.filter.category = event;
    }
    else {
      this.filter.brand = event.target.value;
    }
      
    this.fetchUsers();
  }

  onSearchCancel(event) {
    this.users = [];
    this.loadingCtrl.dismiss();
  }

  onSearchConnections(address) {
      this.loadingCtrl.present();
      this.userProvider.searchConnections(this.currentVision).then(
        data => {
          this.loadingCtrl.dismiss();
          this.users = data
        },
        error => {
          console.log(error),
          this.loadingCtrl.dismiss();
        }
      )
    
  }

  setLocation() {
    if (this.currentVision !== "buyer") return;
   // console.log("setlocation", this.user);

    // try {
    //   const address = this.authUser.person_buyer.addresses[0];
    //   this.locationInput =
    //     address.district.name || address.city.name || address.state.name || "";
    // } catch (err) {
    //   console.warn("setLocation err", err);
    // }
  }

  setupFilterText() {
    if (this.currentVision === "provider") {
      this.searchText1 = "Selecione o ramo";
      this.searchText2 = "";
    } else {
      this.searchText1 = "Digite o produto";
      this.searchText2 = "Buscar por local";
    }
  }

  addOccupationArea() {
    const modal = this.modalCtrl.create("SelectAddressModalPage");
    modal.onDidDismiss(data => {
      this.onDismissModal(data)
    });
    modal.present();
  }

  // removeOccupationArea(address) {

  //   if (this.currentVision === 'provider' && this.formModel.provider)
  //   for (let i in this.formModel.provider.adresses) {
  //     if (address.neighborhood === this.formModel.provider.adresses[i].neighborhood) {
  //       this.formModel.provider.adresses.splice(parseInt(i), 1);
  //     }
  //   }
  //   else if (this.currentVision === 'buyer' && this.formModel.buyer) {
  //     for (let i in this.formModel.buyer.adresses) {
  //       if (address.neighborhood === this.formModel.buyer.adresses[i].neighborhood) {
  //         this.formModel.buyer.adresses.splice(parseInt(i), 1);
  //       }
  //     }
  //   }
  // }

  onDismissModal(data) {
    if (data.state && data.city && data.district) {
      let address = {
        state: data.state.name,
        city: data.city.name,
        neighborhood: data.district.name
      }

      this.filter = {...this.filter, ...address}

      this.fetchUsers();
    }
  }

  fetchBranchOfActivities() {

    this.userProvider.getBranchOfActivities().then(
      response => {
        let branchs = [{ name: "", displayName: "Selecione o ramo"}]
        for (let branch of response) {
          let helper = {name: branch.name, displayName: branch.name}
          branchs.push(helper);
        }
        this.loadingCtrl.dismiss();
        this.branchOfActivities = branchs
      }
    )
    .catch(error => console.log(error)
    )


    // this.userProvider
    //   .getBranchOfActivities()
    //   .then((result: any) => {
    //     let branchs = [
    //       {
    //         name: "",
    //         displayName: "Selecione o ramo"
    //       }
    //     ];

    //     for (let branch of result.data) {
    //       let helper = {
    //         name: branch.name,
    //         displayName: branch.name
    //       };

    //       branchs.push(helper);
    //     }

    //     this.branchOfActivities = branchs;
    //     console.log("branchOfActivities", this.branchOfActivities);
    //   })
    //   .catch((error: any) => {
    //     console.warn("fetchBranchOfActivities error", error);
    //   });
  }

  fetchUsers() {   

    // this.loadingCtrl.present();
    this.userProvider
      .getAll(this.currentVision, this.filter)
      .then((response: any) => {  
        this.users = response;
        this.loadingCtrl.dismiss();
      })
      .catch(error => {       
        console.warn("getConnections error", error);
        this.loadingCtrl.dismiss();
      });
    
    this.loadingCtrl.dismiss();
  }


  getListItemImgSrc(user) {
    let src = "assets/imgs/img_avatar.png";

    if (user.user.image) {
      src = user.user.image.path;
    }

    return src;
  }

  getListItemSubtitle(user) {
    
    let subtitle = "";

    if (this.currentVision === "provider") {
      subtitle = user.user.company_name || "";
    } else {
      let isFirstIteration = true;

      try {
        user.brands.forEach(brand => {
          if (isFirstIteration) {
            isFirstIteration = false;

            subtitle = subtitle + `${brand.name}`;
          } else {
            subtitle = subtitle + `, ${brand.name}`;
          }
        });
      } catch (error) {
        //console.warn('getListItemSubtitle error', error);
      }
    }

    return subtitle;
  }
  getFriendRole() {
    if (this.currentVision == 'provider') return 'buyer'
    if (this.currentVision == 'buyer') return 'provider'
  }

  addConnectionRequest(user) {
    this.loadingCtrl.present();

    const friendId = user.id
    
    
    this.userProvider
      .addConnectionRequest(this.user[this.currentVision].id , friendId, this.getFriendRole())
      .then((response: any) => {
        this.presentToast("Seu pedido de conexão foi enviado!");
        this.fetchUsers()
        this.loadingCtrl.dismiss();
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        this.presentToast("Ocorreu um erro, tente novamente.");
      });
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  updateConnectionStatus(userId, friendId, status) {
    this.userProvider
      .updateConnectionStatus(userId, friendId, status)
      .then(response => {
        console.log("updateConnectionStatus", response);
      })
      .catch(error => {
        console.log("updateConnectionStatus", error);
      });
  }

  onClickUser(user) {
    if (user.connectionStatus === 1) {
      this.navCtrl.push("ViewProfilePage", { userId: user.id });
    }
  }

  onPendentStatusPress(user) {
    if (
      user.parent_users.length > 0 &&
      user.parent_users[0].id === this.user.id
    ) {
      status = user.parent_users[0]._joinData.status;
      const userId = user.child_users[0]._joinData.user_id;
      const friendId = user.child_users[0]._joinData.friend_id;
      this.updateConnectionStatus(userId, friendId, 0);
    } else if (
      user.child_users.length > 0 &&
      user.child_users[0].id === this.user.id
    ) {
      const userId = user.child_users[0]._joinData.user_id;
      const friendId = user.child_users[0]._joinData.friend_id;
      this.updateConnectionStatus(userId, friendId, 0);
    }

    this.users = this.users.map(o => {
      if (o.id === user.id) {
        o.connectionStatus = 0;
      }

      return o;
    });
  }

  onSegmentChange(event) {
    this.currentSegmentPage = event.value;

    if (event.value === "map" && !this.mapIsLoaded) {
      this.loadMap();
    }
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then(
      position => {
        let latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions
        );
      },
      err => {
        console.error(err);
      }
    );
  }

  openViewProfile(user: any) {

    // if (user.person.type != 'corporate') {
    //   return;
    // }

    this.navCtrl.push("ViewProfilePage", { userId: user.id });
  }
}
