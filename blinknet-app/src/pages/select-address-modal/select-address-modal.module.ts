import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectAddressModalPage } from './select-address-modal';

@NgModule({
  declarations: [
    SelectAddressModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectAddressModalPage),
  ],
})
export class SelectAddressModalPageModule {}
