import { Component } from "@angular/core";
import { IonicPage, NavParams, ViewController } from "ionic-angular";
import { AddressModel } from "../../app/models/address";
import { CityModel } from "../../app/models/city";
import { StateModel } from "../../app/models/state";
import { DistrictModel } from "../../app/models/district";

import { StateProvider } from "../../providers/state/state";
import { AddressProvider } from "../../providers/address/address";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-select-address-modal",
  templateUrl: "select-address-modal.html"
})
export class SelectAddressModalPage {
  address: AddressModel;
  cities: CityModel[] = [];
  states: StateModel[] = [];
  districts: DistrictModel[] = [];

  constructor(
    private navParams: NavParams,
    private view: ViewController,
    private stateProvider: StateProvider,
    private addressProvider: AddressProvider,
    public loadingCtrl: LoadingProvider
  ) {
    this.address = AddressModel.GetNewInstance();
  }

  ionViewDidEnter() {
    this.fetchStates();
  }

  onSubmit() {

    const state = this.states.find(o => o.id === this.address.state_id);

    if (state) {
      this.address.state.id = state.id;
      this.address.state.name = state.name;
    }

    const city = this.cities.find(o => o.id === this.address.city_id);

    if (city) {
      this.address.city.id = city.id;
      this.address.city.name = city.name;
    }

    const district = this.districts.find(
      o => o.id === this.address.district_id
    );

    if (district) {
      this.address.district.id = district.id;
      this.address.district.name = district.name;
      this.address.district.city_id = district.city_id;

      if (this.address.district_id === 'Todos os bairros') {
        this.address.district.name = this.address.district_id;
      }
    }

    this.view.dismiss(this.address);
  }

  onSelectChange(type) {
    switch (type) {
      case "state":
        this.address.city_id = null;
        this.address.district_id = null;

        if (this.address.state_id === 'Todos os estados') {
          this.address.state.name = 'Todos os estados';
          this.address.city_id = this.address.city.name = 'Todas as cidades';
          this.address.district_id = this.address.district.name = 'Todos os bairros';
          break;
        }

        this.fetchCities(parseInt(this.address.state_id));
        break;

      case "city":
        this.address.district_id = null;

        if (this.address.city_id === 'Todas as cidades') {
          this.address.city_id = this.address.city.name = 'Todas as cidades';
          this.address.district_id = this.address.district.name = 'Todos os bairros';
          break;
        }

        this.fetchDistricts(parseInt(this.address.city_id), parseInt(this.address.state_id));
        break;

      default:
        break;
    }
  }

  dismiss() {
    this.view.dismiss(null);
  }

  fetchStates() {
    this.loadingCtrl.present();

    this.stateProvider
      .getAll()
      .then((result: any) => {
        this.states = result.data;
        this.loadingCtrl.dismiss();
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        console.warn("fetchStates error", error);
      });
  }

  fetchCities(state_id: number = null) {
    this.loadingCtrl.present();

    this.addressProvider
      .getCities(state_id)
      .then((result: any) => {
        this.loadingCtrl.dismiss();
        this.cities = result.data;
      })
      .catch(error => {
        console.warn("getCities error", error);
        this.loadingCtrl.dismiss();
      });
  }

  fetchDistricts(city_id: number = null, stateId: number = null) {
    this.loadingCtrl.present();

    this.addressProvider
      .getDistricts(city_id, stateId)
      .then((result: any) => {
        this.loadingCtrl.dismiss();

        this.districts = result.data;
      })
      .catch(error => {
        console.warn("getCities error", error);
        this.loadingCtrl.dismiss();
      });
  }
}
