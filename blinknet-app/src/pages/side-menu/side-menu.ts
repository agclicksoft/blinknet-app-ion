import { FeedPage } from './../feed/feed';
//import { TabsPage } from '../tabs/tabs';
import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, Nav, NavParams } from "ionic-angular";
import { SocialSharing } from "@ionic-native/social-sharing";
import { Storage } from "@ionic/storage";

import { AuthProvider } from "../../providers/auth/auth";
import { User } from '../../app/models/user2';
import { LoginPage } from '../login/login';

export interface PageInterface {
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
  icon: string;
}

@IonicPage()
@Component({
  selector: "page-side-menu",
  templateUrl: "side-menu.html"
})
export class SideMenuPage {
  // Basic root for our content view
  rootPage = "TabsPage";
  authUser: User;
  currentVision: string = "";

  @ViewChild(Nav)
  nav: Nav;

  pages: PageInterface[] = [
    {
      title: "Novidades",
      pageName: "FeedPage",
      tabComponent: "FeedPage",
      index: 0,
      icon: "paper"
    },
    {
      title: "Conexões",
      pageName: "ConnectionsPage",
      tabComponent: "ConnectionsPage",
      index: 1,
      icon: "people"
    },
    {
      title: "Busca",
      pageName: "SearchPage",
      tabComponent: "SearchPage",
      index: 2,
      icon: "search"
    },
    // {
    //   title: "Pedidos",
    //   pageName: "RequestsPage",
    //   tabComponent: "RequestsPage",
    //   index: 3,
    //   icon: "person"
    // },
    {
      title: "Perfil",
      pageName: "ProfilePage",
      tabComponent: "ProfilePage",
      index: 3,
      icon: "person"
    }
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authProvider: AuthProvider,
    private socialSharing: SocialSharing,
    private storage : Storage
  ) {
    //this.authUser = AuthenticatedUser.GetNewInstance();
  }

  ionViewDidEnter() {
    //this.loadAuthUserData();
  }

  async loadAuthUserData() {
    //if (await !this.authProvider.isAuthenticated()) return;
    await this.storage.get('user').then((user) => this.authProvider = JSON.parse(user))    

    //this.currentVision = this.authProvider.getCurrentVision();

    if (this.authUser.corporate != null) {
      this.pages = [{
          title: "Novidades",
          pageName: "FeedPage",
          tabComponent: "FeedPage",
          index: 0,
          icon: "paper"
        },
        {
          title: "Perfil",
          pageName: "ProfilePage",
          tabComponent: "ProfilePage",
          index: 1,
          icon: "person"
        }
      ];
    }
  }

  openPage(page: PageInterface) {
    let params = {};

    // The index is equal to the order of our tabs inside tabs.ts
    if (page.index) {
      params = { tabIndex: page.index };
    }

    console.log(page);

    // The active child nav is our Tabs Navigation
    if (this.nav.getActiveChildNavs().length > 0 && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    } 
    else {
      // Tabs are not active, so reset the root page
      // In this case: moving to or from SpecialPage
      this.nav.setRoot(page.pageName, params);
    }
  }

  isActive(page: PageInterface) {
    // Again the Tabs Navigation
    let childNav = this.nav.getActiveChildNavs().length > 0 ? this.nav.getActiveChildNavs()[0] : null;

    if (childNav) {
      if (
        childNav.getSelected() &&
        childNav.getSelected().root === page.tabComponent
      ) {
        return "primary";
      }
      return;
    }

    // Fallback needed when there is no active childnav (tabs not active)
    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return "primary";
    }
    return;
  }

  openShareSheet() {
    const msg = "Você conhece o aplicativo que conecta vendedores, compradores e indústrias de todo Brasil? Baixe para Android gratuitamente! ";
    this.socialSharing.share(
      msg,
      "Blinknet",
      null,
      "https://play.google.com/store/apps/details?id=br.com.clicksoft.blinknet"
    );
  }

  openFeed() {
    this.nav.push(FeedPage);
  }

  logout() {
    this.authProvider.logout();
    this.navCtrl.setRoot('LoginPage', {}, { animate: true, direction: "back" });
  }
}
