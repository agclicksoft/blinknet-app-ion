import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: "page-tabs",
  templateUrl: "tabs.html"
})
export class TabsPage {
  tab1Root: any = "FeedPage";
  tab2Root: any = "ConectionsPage";
  tab3Root: any = "SearchPage";
  tab4Root: any = "RequestsPage";
  tab5Root: any = "ProfilePage";

  myIndex: number;
  currentVision: string = "none";

  constructor (
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AuthProvider
  ) {
    this.currentVision = this.authProvider.getCurrentVision();
    console.log(this.currentVision);
    this.myIndex = navParams.data.tabIndex || 0;
  }
}
