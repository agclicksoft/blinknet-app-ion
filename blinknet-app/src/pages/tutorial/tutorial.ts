import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Platform, MenuController, Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

export interface Slide {
  index: number;
  title: string;
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {
  @ViewChild('myslider') slider: Slides;
  slides: Slide[];
  showSkip = true;
  dir: string = 'ltr';

  constructor(public navCtrl: NavController, 
              public menu: MenuController, 
              public platform: Platform,
              private storage: Storage,
              private androidFullScreen: AndroidFullScreen) {

    this.androidFullScreen.isImmersiveModeSupported()
      .then(() => this.androidFullScreen.immersiveMode())
      .catch((error: any) => console.log(error));

    this.storage.get("FIRST_ACCESS").then((data) => {
      if (data && data == "0") {
        this.startApp();
      }
    });

    this.dir = platform.dir();

    this.slides = [
      {
        index: 0,
        title: "Comprador",
        description: "Você que faz as compras da sua empresa tem um canal direto com os fornecedores. Crie sua rede de revendedores e representantes, agilize suas negociações e receba promoções. Compre melhor!",
        image: 'assets/imgs/comprador_tutorial1.PNG',
      },
      {
        index: 1,
        title: "Vendedor",
        description: "Encontre compradores da sua região e conecte-se via Whatsapp, Telefone ou Email. Poste sua promoções para um público direcionado que compra o seu produto. Com o app Blinknet você pode vender mais!",
        image: 'assets/imgs/vendedor_tutorial1.PNG',
      },
      {
        index: 2,
        title: "Corporativo",
        description: "Agora você tem um canal para divulgar promoções e novidades da sua empresa de forma direcionada. Atinja em uma única plataforma um público de vendedores e compradores, que vai desde representantes comerciais até comerciantes locais de todo Brasil.",
        image: 'assets/imgs/corporativo_tutorial1.PNG',
      }
    ];
  }

  startApp() {
    this.menu.enable(true);
    this.navCtrl.setRoot('HomePage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

  next(index) {
    if (index < 2) {
      this.slider.slideTo(index + 1, 0);
    } else {
      this.storage.set("FIRST_ACCESS", "0").then(() => {
        this.startApp();
      })
    }
  }

}
