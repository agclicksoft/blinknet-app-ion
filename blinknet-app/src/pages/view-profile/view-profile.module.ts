import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewProfilePage } from './view-profile';
import { RlTagInputModule } from 'angular2-tag-input/dist';
import { DirectivesModule } from "../../directives/directives.module";
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    ViewProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewProfilePage),
    DirectivesModule,
    RlTagInputModule,
    BrMaskerModule
  ],
})
export class ViewProfilePageModule {}
