import { Component } from "@angular/core";
import {
  Platform,
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { SocialSharing } from "@ionic-native/social-sharing";
import { CallNumber } from "@ionic-native/call-number";
import { Geolocation } from "@ionic-native/geolocation";

import { AddressModel } from "../../app/models/address";
import { UserModel } from "../../app/models/user";
import { AuthenticatedUser } from "../../app/models/authenticated.user";

import { UtilProvider } from "../../providers/util/util";
import { StateProvider } from "../../providers/state/state";

import { AuthProvider } from "../../providers/auth/auth";
import { UserProvider } from "../../providers/user/user";
import { LoadingProvider } from "../../providers/loading/loading";
import { User } from "../../app/models/user2";

@IonicPage()
@Component({
  selector: "page-view-profile",
  templateUrl: "view-profile.html"
})
export class ViewProfilePage {

  user: User;
  userProfileId: number;
  userProfile: User = new User();

  authUser: AuthenticatedUser;
  userModel: UserModel;
  form: FormGroup;
  profileImgSrc: string = "assets/imgs/img_avatar.png";
  loadingSpinner: any;

  formModel: any;
  buyerAddress: AddressModel;
  corporateAddress: AddressModel = null;

  currentVision: string;
  states: any[] = [];
  branchOfActivities: any[] = [];
  buttonText: string;
  shouldShowNumOfBoxes: boolean = false;

  provider_product_categories: string[] = ["lala", "lele"];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private platform: Platform,
    private authProvider: AuthProvider,
    private userProvider: UserProvider,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingProvider,
    private socialSharing: SocialSharing,
    private callNumber: CallNumber,
    private utilProvider: UtilProvider,
    private stateProvider: StateProvider
  ) {
    this.currentVision = this.authProvider.getCurrentVision();
    this.authUser = AuthenticatedUser.GetNewInstance();
    this.userModel = UserModel.GetNewInstance();
    this.formModel = UserModel.GetNewInstance();
    this.buyerAddress = AddressModel.GetNewInstance();

    this.form = this.formBuilder.group({
      company_name: ["", Validators.compose([Validators.required])],
      cnpj: ["", Validators.compose([Validators.required])],
      occupation_areas: [[], Validators.compose([Validators.required])],
      brands: [[], Validators.compose([Validators.required])],
      product_categories: [[], Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    //this.fetchStates();
    //this.fetchBranchOfActivities();
  }

  ionViewDidEnter() {
    this.user = this.authProvider.user;
    this.currentVision = this.user.currente_role;
    this.userProfileId = this.navParams.get("userId");
    this.loadAuthUserData(this.userProfileId);
    this.getUserProfile();
  }

  getUserProfile() {
    this.loadingCtrl.present();
    this.userProvider.getOne(this.userProfileId).then(
      response => {
        let res: any = response;
        this.userProfile = res.data;
        console.log(this.userProfile);
        this.loadingCtrl.dismiss();
      },
      error => {
        console.log(error);
        this.loadingCtrl.dismiss();
      }
    )
    

  }

  async loadAuthUserData(userId) {
    // this.loadingCtrl.present();
    // this.authUser = await this.authProvider.getOnStorage();

    // this.userProvider.getOne(userId).then((response: any) => {

    //     this.userModel = UserModel.ParseFromObject(response.data);
    //     if (this.userModel.avatar_base64) {
    //       this.profileImgSrc = this.userModel.avatar_base64;
    //     }
    //     this.setFormModel(response.data);
    //     this.loadingCtrl.dismiss();
    //   })
    //   .catch(error => {
    //     this.loadingCtrl.dismiss();
    //     console.error("error", error);
    //     this.presentToast("Erro ao carregar os dados");
    //   });
  }

  setFormModel(userModel) {
    this.formModel = UserModel.ParseFromObject(userModel);

    if (this.utilProvider.isSet(this.formModel.avatar_base64)) {
      this.profileImgSrc = this.formModel.avatar_base64;
    }

    try {
      if (this.formModel.buyer.adresses.length > 0) {
        const firstAddress = this.formModel.person_buyer.addresses[0];
        this.buyerAddress = AddressModel.ParseFromObject(firstAddress);
      }

      if (this.formModel.corporate.addresses.length > 0) {
        const firstAddress = this.formModel.person_corporate.addresses[0];
        this.corporateAddress = AddressModel.ParseFromObject(firstAddress);

      }

      if (userModel.buyer) {
        this.branchOfActivities = userModel.buyer.categories;
      }

      if (userModel.provider) {
        this.formModel.provider.brands = userModel.provider.brands.map(
          o => o.name
        );

        this.formModel.provider.categories = userModel.provider.categories.map(
          o => o.name
        );
      }

      if (userModel.corporate) {
        this.formModel.corporate.brands = userModel.corporate.brands.map(
          o => o.name
        );

        this.formModel.corporate.categories = userModel.corporate.categories.map(
          o => o.name
        );
      }
    } catch (error) {
        console.warn("setFormModel error", error);
    }

    console.log("this.branchOfActivities", this.branchOfActivities);
    //console.log("setFormModel this.formModel", this.formModel);
    this.onCepBlur(null);
    this.setButtonText();
  }

  getProductCategories() {
    const categories = this.formModel.provider.categories;
  }

  onCepBlur(event) {
    console.log("onCepBlur -> formModel-> ", this.formModel);

    let zipCode;

    if (this.formModel.person_buyer.addresses.length > 0) {
      zipCode = this.formModel.person_buyer.addresses[0].zip_code;
    }

    console.log("zipCode", zipCode);

    if (this.utilProvider.isSet(zipCode) && zipCode.length > 5) {
      this.loadingCtrl.present();
      let zipCodeSanitazed = zipCode.replace(/\D/g, "");

      this.userProvider
        .getCepData(zipCodeSanitazed)
        .then((result: any) => {
          this.loadingCtrl.present();

          if (this.formModel.person_buyer && this.formModel.person_buyer.buyer) {
            //this.buyerAddress.street = result.logradouro;
            this.buyerAddress.district.name = result.bairro;
            this.buyerAddress.city.name = result.localidade;

            const stateId = this.getIdForStateName(result.localidade);
            this.buyerAddress.state_id = stateId;
            this.buyerAddress.state.id = stateId;
            this.buyerAddress.state.name = result.localidade;

            console.log("this.buyerAddress", this.buyerAddress);
          }

          if (this.formModel.person_corporate && this.formModel.person_corporate.buyer) {
            // Corporate nao exibe endereco
          }
        })
        .catch(error => {
          this.loadingCtrl.dismiss();
          console.error("onCepBlur", error);
        });
    }
  }

  setButtonText() {
    if (this.currentVision === "provider") {
      if (this.formModel.person_buyer.buyer.id !== null) {
        this.buttonText = "Trocar para comprador";
      } else {
        this.buttonText = "Cadastrar comprador";
      }
    } else {
      if (this.formModel.person_provider.provider.id !== null) {
        this.buttonText = "Trocar para vendedor";
      } else {
        this.buttonText = "Cadastrar vendedor";
      }
    }
  }

  getIdForStateName(stateName) {
    for (const state of this.states) {
      if (state.name.includes(stateName)) return state.id;
    }

    return null;
  }

  onFormSubmit() {}

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  openWhatsApp(phoneNumber) {
    console.log("shareViaWhatsApp");

    let myphoneNumber = phoneNumber.replace(" ", "");
    myphoneNumber = myphoneNumber.replace("-", "");

    if (this.platform.is("ios")) {
      const url = `https://api.whatsapp.com/send?phone=55${myphoneNumber}`;
      window.open(url, "_system");
    } else {
      this.socialSharing
        .shareViaWhatsAppToReceiver(phoneNumber, null, null, null)
        .then(result => {
          console.log("shareViaWhatsAppToReceiver", result);
        })
        .catch(error =>
          console.warn("shareViaWhatsAppToReceiver error", error)
        );
    }
  }

  openEmail(email) {
    console.log("email", email);

    this.socialSharing
      .canShareViaEmail()
      .then(() => {
        console.log("can share via email");
        this.socialSharing
          .shareViaEmail("", "Contato via app Blinknet", [email])
          .then(result => {
            console.log("shareViaEmail", result);
          })
          .catch(error => console.log("shareViaEmail", error));
      })
      .catch(() => {
        console.log("cant share via email");
      });
  }

  openShareSheet() {
    const msg = "";
    this.socialSharing.share(
      msg,
      "Blinknet",
      null,
      "http://blink.whmserver.net/api/users/redirectToAppStore"
    );
  }

  callPhoneNumber(phoneNumber) {
    this.callNumber
      .callNumber(phoneNumber, true)
      .then(res => console.log("Launched dialer!", res))
      .catch(err => console.log("Error launching dialer", err));
  }
}
