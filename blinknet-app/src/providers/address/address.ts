import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StateModel } from "../../app/models/state";

@Injectable()
export class AddressProvider {
  //produção
  url = "https://appblinknet.herokuapp.com/api";
  cityUrl = "https://appblinknet.herokuapp.com/api/cities";
  districtUrl = "https://appblinknet.herokuapp.com/api/districts";
  //localhost
  // url = "http://localhost:8765/api";
  // cityUrl = "http://localhost:8765/api/cities";
  // districtUrl = "http://localhost:8765/api/districts";
  states: StateModel[] = [];

  constructor(public http: HttpClient) {}

  getStates() {
    return new Promise(resolve => {
      if (this.states.length > 0) {
        const data = {
          status: "ok",
          data: this.states
        };
        return resolve(data);
      }

      this.http.get(this.url + "/states").subscribe(
        (response: any) => {
          if (response.status === "ok") this.states = response.data;
          resolve(response);
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  getCepData(cep) {
    let url = `https://viacep.com.br/ws/${cep}/json/`;

    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }

  getStatesCities() {
    return new Promise((resolve, reject) => {
      this.http.get("assets/data/states-cities.json").subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }

  getCities(stateId: number = null) {
    const url = this.cityUrl + "?stateId=" + stateId;

    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }

  getCity(cityId: number = null, name: string = null) {
    const url = `${this.cityUrl}/view?cityId=${cityId || ""}&name=${name ||
      ""}`;

    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.error(err);
          reject(err);
        }
      );
    });
  }

  getDistricts(cityId: number = null, stateId: number = null) {
    const url = `${this.districtUrl}?cityId=${cityId || ""}&stateId=${stateId || ""}`;
      console.log('url: '+url);
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        data => {
          console.log(data);

          resolve(data);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }

  getDistrict(stateId: number = null, name: string = null) {
    const url = `${this.districtUrl}/view?stateId=${stateId ||
      ""}&name=${name || ""}`;

    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }

  getLatLong(zip_code) {
    const apiKey = "AIzaSyANqZ5QWoIa7oAzA9uZh1dvWZahl80Oky4";
    const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${zip_code}&key=${apiKey}`;

    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }
}
