import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Injectable, ReflectiveInjector } from "@angular/core";
import { AuthenticatedUser } from "../../app/models/authenticated.user";
import { Storage } from "@ionic/storage";
import { UtilProvider } from "../util/util";
import { User } from "../../app/models/user2";

@Injectable()
export class AuthProvider {
  url = "https://blinknet-api.herokuapp.com";
  //url = "https://appblinknet.herokuapp.com/api";
  //url = "http://localhost:8765/api";

  public user: User;
  public currentVision: string = null;

  constructor(
    public http: HttpClient,
    private storage: Storage,
    private util: UtilProvider
  ) {
    this.storage.get('user').then(
      response => {
        console.log(response);
        this.user = JSON.parse(response),
        this.currentVision = this.user.currente_role;
      }
    )                                                                                                    
  }

  initAuth() {
    return 'Auth has been started';
  }

  signIn(email, password) {
    let url = this.url + "/v1/auth/authenticate";

    let data = { email, password };

    return this.http.post<any>(url, data);;
  }

  signUp(email, password) {
    let params = { email, password };
    let options = {
      headers: new HttpHeaders()
        .set("Accept", "application/json")
        .set("Content-Type", "application/json"),

      params: new HttpParams()
    };

    return new Promise((resolve, reject) => {
      this.http.post(this.url + "/users/token", params, options).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.error(err);
          reject(err);
        }
      );
    });
  }

  async initCurrentVision() {
    await this.storage.get('user').then( data => { this.user = JSON.parse(data), console.log(data)});
    this.currentVision = this.user.currente_role
    return this.currentVision;
  }

  setCurrentVision(value, id) {
    this.currentVision = value;
    
    this.storage.get('token').then(
      token => {  
        console.log('tk feed', token)
        let headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
          
        });

      this.http.put<any>(`${this.url}/v1/auth/update-role`, {currente_role: value}).subscribe(
        response => {
          this.user = response.data,
          this.storage.set('user', JSON.stringify(response.data))
          console.log(this.user)
        },
        error => console.log(error)
      )
    }),
    error => {
      console.log(error)
    }
  }

  updateUserService(user: any) {
    this.user = user;
    this.initCurrentVision();
  }

  getCurrentVision() {
    return this.currentVision;
  }

  register(user: any) {
    const url = this.url + "/v1/auth/register";

    return this.http.post<any>(url, user);
  }

  recuperatePassword(email) {
    let params = { email };

    return new Promise((resolve, reject) => {
      this.http.post(this.url + "/users/recuperatePassword", params).subscribe(
        response => {
          resolve(response);
        },
        err => {
          console.error(err);
          reject(err);
        }
      );
    });
  }

  async logout() {
    await this.storage.clear();
    this.user = null;
  }
}
