import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()

export class CommonProvider {
    url = "https://blinknet-api.herokuapp.com/v1";

    constructor (
        public http: HttpClient,
        private storage: Storage
        
    ) {}

    setRole(data, role) {
        return this.http.post<any>(`${this.url}/${role}`, data);
    }

    updateUser(data, role, id) : Promise<any> {
        return new Promise((resolve, reject) => {
            this.storage.get('token').then(token => {
                let headers = new HttpHeaders({
                    Authorization: `Bearer ${token}`
                });
                this.http.put<any>(`${this.url}/${role}/${id}`, data, {headers} ).subscribe(
                    data => resolve(data)
                    ,error => reject(error)
                )
            })          
        })
        
    }

    updateImageProfile(data, id) {
        return this.http.post<any>(`${this.url}/users/${id}/upload-image`, data);
    }

}