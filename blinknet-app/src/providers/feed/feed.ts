import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class FeedProvider {
  token: string;
  url = "https://blinknet-api.herokuapp.com";
  //url = "http://localhost:8765/api/feeds";

  constructor(
    public http: HttpClient,
    private storage: Storage
    ) {}

  add(feed: any) {
    const url = this.url + "/v1/products";
    console.log("URL Registro: " + url);

    return new Promise((resolve, reject) => {
      this.http.post(url, feed).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.error(err);
          reject(err);
        }
      );
    });
  }
  async uploadImg(picture: FormData, idProduct) {
    const url = this.url + `/v1/products/${idProduct}/upload-image`;
    await this.storage.get('token').then((token) => {
      return new Promise((resolve, reject) => {
        this.http.post(url, picture).subscribe(data => {
          resolve(data)
        }, error => {
          reject(error)
        });
      });

    })
}

  getAll(pageNumber) {
    //TODO: Fazer nova requisição a lista de feeds
    const url = this.url + "/v1/products?page=" + pageNumber;

    return new Promise((resolve, reject) => {

      this.storage.get('token').then(
        token => {  
          console.log('tk feed', token)
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
        
          this.http.get(url, {headers}).subscribe(
            data => { resolve(data); },
            err => { console.error(err); reject(err); }
          );
        }
      )

    });    
  }

  findAdress(cep) {
    let url = `http://viacep.com.br/ws/${cep}/json/`

    return new Promise((resolve, reject) => {
      this.http
        .get(url)
        .timeout(10000)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.error(err);
            reject(err);
          }
        );
    });
  }

  getByUserId(id, page) {

    let url = this.url + "/getbyuserid?id=" + id;

    if (page) {
      url = url + "&page=" + page;
    }
    console.log('url: ' + url);
    return new Promise((resolve, reject) => {
      this.http
        .get(url)
        .timeout(10000)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.error(err);
            reject(err);
          }
        );
    });
  }

  delete(id) {
    const url = this.url + "/delete?id=" + id;

    return new Promise((resolve, reject) => {
      this.http
        .get(url)
        .timeout(10000)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.error(err);
            reject(err);
          }
        );
    });
  }

}
