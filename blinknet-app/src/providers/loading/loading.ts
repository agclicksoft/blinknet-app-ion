import { Injectable } from "@angular/core";
import { Loading, LoadingController } from "ionic-angular";

@Injectable()
export class LoadingProvider {
  loading: Loading;
  constructor(public loadingCtrl: LoadingController) {
    this.loading = null;
  }

  presentWithGif1() {
    this.loading = this.loadingCtrl.create({
      spinner: "hide",
      content: `
          <div class="custom-spinner-container">
            <img class="loading" width="120px" height="120px" src="assets/loader1.gif" />
          </div>`
    });

    return this.loading.present();
  }

  present() {
    this.loading = this.loadingCtrl.create({});

    setTimeout(() => this.dismiss(), 4000);

    return this.loading.present();
  }

  presentWithMessage(message) {
    this.loading = this.loadingCtrl.create({
      content: message
    });

    return this.loading.present();
  }

  dismiss() {
    if (this.loading) {
      try {
        this.loading.dismissAll();
        this.loading = null;
      } catch (error) {
        console.warn(error);
        this.loading = null;
      }
    }
  }
}
