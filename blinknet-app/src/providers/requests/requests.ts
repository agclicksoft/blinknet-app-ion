import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class RequestsProvider {
  url = "https://blinknet-api.herokuapp.com";
  //url = "http://localhost:8765/api/feeds";

  constructor(
    public http: HttpClient,
    private storage: Storage
    ) {
    console.log('Hello RequestsProvider Provider');
  }


  getAll(role, id, page): Promise<any> {
    const url = this.url + `/v1/${role}/${id}/orders?page=${page}`;

    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
          this.http.get<any>(url, { headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }

  getByUserId(id, page) {

    let url = this.url + "/getbyuserid?id=" + id;

    if (page) {
      url = url + "&page=" + page;
    }
    console.log('url: ' + url);
    return new Promise((resolve, reject) => {
      this.http
        .get(url)
        .timeout(10000)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.error(err);
            reject(err);
          }
        );
    });
  }

  delete(id) {
    const url = this.url + "/delete?id=" + id;

    return new Promise((resolve, reject) => {
      this.http
        .get(url)
        .timeout(10000)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.error(err);
            reject(err);
          }
        );
    });
  }

}
