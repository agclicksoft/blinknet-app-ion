import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
//import { UserModel } from "../../app/models/state";

@Injectable()
export class StateProvider {
  url = "https://appblinknet.herokuapp.com/api/states";
  //url = "http://localhost:8765/api/states";
  
  constructor(public http: HttpClient) {
    console.log("Hello StatesProvider Provider");
  }

  getAll() {
    let options = {
      headers: new HttpHeaders()
        .set("Accept", "application/json")
        .set("Content-Type", "application/json"),

      params: new HttpParams()
    };

    return new Promise(resolve => {
      this.http.get(this.url, options).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.error(err);
        }
      );
    });
  }
}
