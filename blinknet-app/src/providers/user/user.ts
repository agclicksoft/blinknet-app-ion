import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/timeout";


import { AuthProvider } from "../auth/auth";
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { Storage } from "@ionic/storage";

@Injectable()
export class UserProvider {
  url = "https://blinknet-api.herokuapp.com";
  //url = "http://localhost:8765/api/users";

  branchOfActivities: any[] = [];
  pendingSolicitations = [];
  pendingConnections = [];
  approvedConnections = [];

  constructor(
    public http: HttpClient,
    private authProvider: AuthProvider,
    private transfer: FileTransfer,
    private storage: Storage
  ) {

  }

  // get(id) {
  //   let url = this.url + "/" + id;

  //   return new Promise((resolve, reject) => {
  //     this.http.get(url).subscribe(
  //       data => {
  //         resolve(data);
  //       },
  //       err => {
  //         console.error(err);
  //         reject(err);
  //       }
  //     );
  //   });
  // }

  register(user) { }

  getOne(id) {
    const url = `${this.url}/v1/users/${id}`


    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {

          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });

          this.http.get(url, { headers: headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        }
      );
    })
  }

  get() {
    let url = `${this.url}/v1/auth/account`
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    });

    return new Promise((resolve, reject) => {
      this.http.get(url, { headers: headers }).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.error(err);
          reject(err);
        }
      );
    });
  }

  edit(id, user: any) {
    //let data = { user: user };
    let url = this.url + "/" + id;

    return new Promise((resolve, reject) => {
      this.http.put(url, user).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.error(err);
          reject(err);
        }
      );
    });
  }

  getAll(userType, filter: any) {

    if(userType === 'provider'){
      filter = `category=${filter.category || ''}&state=${filter.state || ''}&city=${filter.city || ''}&neighborhood=${filter.neighborhood || ''}`;
    }
    else {
      filter = `brand=${filter.brand || ''}&state=${filter.state || ''}&city=${filter.city || ''}&neighborhood=${filter.neighborhood || ''}`;
    }



    console.log(filter);
    const url = `${this.url}/v1/connections/${userType}?${filter}`;
    return new Promise((resolve, reject) => {
      
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
      this.http
        .get(url, {headers})
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.error(err);
            reject(err);
          }
        );
      //.timeout(10000)
    });
  })
}

  getBranchOfActivities(): Promise<any> {
    const url = `${this.url}/v1/categories`;

    //  return this.http.get<any>(url);
    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.get(url, { headers }).subscribe(
            (response: any) => {
              resolve(response);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        }
      )
    });
  }

  //connections
  getConnections(userType: string = "buyer", userId = null): any {


    const url = this.url + `/v1/${userType}s/${userId}/connections`;
    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.get(url, { headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }
  searchConnections(userType: string = "buyer"): Promise<any> {

    const url = this.url + `/v1/connections/${userType}`;

    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          console.log(token);
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.get(url, { headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }
  getConnectionsConfim(userType: string = "buyer"): any {

    const url = this.url + `/v1/connections?search=${userType}`;

    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.get(url, { headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }
  accept(idConnection) {
    const url = this.url + `/v1/connections/${idConnection}`;
    let data = {
      accept: 1,
    };

    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.put(url, data, { headers }).subscribe(

            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }
  reject(idConnection) {
    const url = this.url + `/v1/connections/${idConnection}`;
    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.delete(url, { headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }
  onRegisterOrder(idProduct) {
    const url = this.url + `/v1/products/${idProduct}/orders`;
    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.post(url, {}, { headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }

  updateConnectionStatus(userId, friendId, status) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    });

    const url = this.url + "/updateConnectionStatus";
    let data = {
      userId: userId,
      friendId: friendId,
      status: status
    };

    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.post(url, data, { headers: headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }

  addOrderRequest(amount, productId) {
    const url = this.url + `/v1/products/${productId}/orders`;
    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.post(url, {amount: amount}, { headers }).subscribe(
            data => {
              resolve(data);
              console.log(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }

  addConnectionRequest(userRoleId, friendId, friendRole, amount = 0, productId = null) {
    const url = this.url + "/v1/connections";
    var id1;
    var id2;
    if (friendRole == 'buyer') {
      id2 = friendId
      id1 = userRoleId
    } else {
      id2 = userRoleId
      id1 = friendId
    }
    let data = {
      provider_id: id1,
      buyer_id: id2,
      amount: amount,
      product_id: productId,
      send_control: userRoleId
    };
    console.log('provider_id', id1)
    console.log('buyer_id', id2)
    console.log('amount', amount)
    console.log('product_id', productId)
    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http.post(url, data, { headers }).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
    })
  }

  getCepData(cep) {
    let url = `https://viacep.com.br/ws/${cep}/json/`;

    return new Promise((resolve, reject) => {
   
          this.http.get(url).subscribe(
            data => {
              resolve(data);
            },
            err => {
              console.error(err);
              reject(err);
            }
          );
        });
  }

  uploadAvatar(filePath, fileName, mimeType, user) {
    const fileTransfer: FileTransferObject = this.transfer.create();
    const url = this.url + "/uploadAvatar";
    let options: FileUploadOptions = {
      fileKey: "photo",
      fileName: fileName,
      mimeType: mimeType,
      headers: {},
      params: {}
    };

    return new Promise((resolve, reject) => {
      fileTransfer
        .upload(filePath, url, options)
        .then(data => resolve(data), err => reject(err));
    });
  }

  updatePassword(userId: number, password: string) {
    const url = `${this.url}/v1/auth/change-password`;
    let data = { password };

    return new Promise((resolve, reject) => {
      this.storage.get('token').then(
        token => {
          let headers = new HttpHeaders({
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`
          });
          this.http
            .post(url, data, { headers })
            .subscribe(data => resolve(data), error => reject(error));
        });      
    })
  }
}
