import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';

/*
  Generated class for the UtilProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilProvider {
    platform: Platform;
    
  constructor() {
    this.platform = new Platform();
  }

  isSet(value: any) {
    return value !== undefined && value !== null && typeof value !== undefined;
  }

  resizeImage(longSideMax, url) {
    console.log("resizeImage was called");
    var tempImg = new Image();

    if (this.platform.is('ios') && this.platform.version().num > 10.2) {        
        tempImg.crossOrigin = 'anonymous';                  
    }
    
    return new Promise((resolve, reject) => {
      tempImg.src = url;
      tempImg.onload = () => {
        console.log("image loaded");
        // Get image size and aspect ratio.
        var targetWidth = tempImg.width;
        var targetHeight = tempImg.height;
        var aspect = tempImg.width / tempImg.height;

        // Calculate shorter side length, keeping aspect ratio on image.
        // If source image size is less than given longSideMax, then it need to be
        // considered instead.
        if (tempImg.width > tempImg.height) {
          longSideMax = Math.min(tempImg.width, longSideMax);
          targetWidth = longSideMax;
          targetHeight = longSideMax / aspect;
        } else {
          longSideMax = Math.min(tempImg.height, longSideMax);
          targetHeight = longSideMax;
          targetWidth = longSideMax * aspect;
        }

        // Create canvas of required size.
        var canvas = document.createElement("canvas");
        canvas.width = targetWidth;
        canvas.height = targetHeight;

        var ctx = canvas.getContext("2d");
        // Take image from top left corner to bottom right corner and draw the image
        // on canvas to completely fill into.
        ctx.drawImage(
          tempImg,
          0,
          0,
          tempImg.width,
          tempImg.height,
          0,
          0,
          targetWidth,
          targetHeight
        );

        resolve(canvas.toDataURL("image/jpeg"));
      };

      tempImg.onerror = reject;
    });
  }
}
